package com.lzt841.script.grammar;

import com.lzt841.script.lexical.Word;

public class NodeGroupHandler implements NodeHandler{
    private int startToken;
    private int endToken;

    public NodeGroupHandler(int startToken, int endToken) {
        this.startToken = startToken;
        this.endToken = endToken;
    }
    @Override
    public boolean equals(Object obj) {
        if(obj==this)return true;
        if(obj instanceof NodeGroupHandler){
            return this.startToken==((NodeGroupHandler) obj).startToken;
        }
        return false;
    }
    public void  handler(CodeNode root){
        handler(root,startToken,endToken);
    }



    private void handler(CodeNode root, int startToken, int endToken) {
        int n=root.childrens.size();

        CodeNode now=root;
        for (int i=0;i < n;i++) {
            CodeNode c=root.childrens.get(i);
            Word word=c.word;
            int token=c.getFirstWordToken();
            if (token==startToken) {
                CodeNode start=new CodeNode();
                start.word=word;
                c.addChildren(start);
                c.word=null;
                if(!now.childrens.contains(c)){
                    now.addChildren(c);
                    n--;i--;
                }
                now = c;

            } else if (now!=null&&token==endToken) {
                now.addChildren(c);
                n--;i--;
                now=now.parent;
            } else if(now!=null) {
                if(!now.childrens.contains(c)) {
                    n--;
                    i--;
                    now.addChildren(c);
                }
                handler(c,startToken,endToken);
            }
        }

    }
}
