package com.lzt841.script.grammar;
import com.lzt841.script.lexical.Word;

public interface NodeHandler {

    void handler(CodeNode root);
    
}
