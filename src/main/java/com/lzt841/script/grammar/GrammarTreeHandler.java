package com.lzt841.script.grammar;

import com.lzt841.script.lexical.Word;

import java.util.List;

public interface GrammarTreeHandler{
    public CodeNode analysis(CharSequence srcCode, List<Word> words);
}
