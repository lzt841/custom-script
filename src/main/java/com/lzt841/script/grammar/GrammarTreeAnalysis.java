package com.lzt841.script.grammar;
import com.lzt841.script.lexical.Word;

import java.util.ArrayList;
import java.util.List;

public class GrammarTreeAnalysis implements GrammarTreeHandler {
    public List<NodeGroupHandler> nodeGroupHandlers=new ArrayList<>();
    public GrammarTreeAnalysis() {

    }



    public boolean addNodeGroupHandler(NodeGroupHandler codeNodeGroupHandler) {
        for (NodeGroupHandler nodeGroupHandler : nodeGroupHandlers) {
            if(nodeGroupHandler.equals(codeNodeGroupHandler)){
                return false;
            }
        }
        nodeGroupHandlers.add(codeNodeGroupHandler);
        return true;
    }

    @Override
    public CodeNode analysis(CharSequence srcCode, List<Word> words) {


        CodeNode root=new CodeNode();
        for (Word word : words) {
            CodeNode node=new CodeNode();
            node.word=word;
            root.addChildren(node);
        }
        for (NodeGroupHandler nodeGroupHandler : nodeGroupHandlers) {
            nodeGroupHandler.handler(root);
        }
        return root;

    }





}
