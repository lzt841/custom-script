package com.lzt841.script.grammar;

import com.lzt841.script.language.ls.utils.KeyWordConfig;
import com.lzt841.script.lexical.Word;

import java.util.ArrayList;
import java.util.List;

public class CodeNode{
    public CharSequence name;
    public CodeNode parent;
    public List<CodeNode> childrens=new ArrayList<>();
    
    public Word word;

    public void addChildren(CodeNode children){
        if(children.parent!=null){
            children.parent.childrens.remove(children);
        }
        children.parent=this;
        childrens.add(children);
    }
    public void addChildren(int index, CodeNode children){
        if(children.parent!=null){
            children.parent.childrens.remove(children);
        }
        children.parent=this;
        childrens.add(index,children);
    }
    public int getFirstWordToken() {
        if(this.word!=null){
            return this.word.token;
        }else if(this.childrens.size()>0){
            return childrens.get(0).getFirstWordToken();
        }
        return -1;
    }
    public String getFirstWordText() {
        if(this.word!=null){
            return this.word.word;
        }else if(this.childrens.size()>0){
            return childrens.get(0).getFirstWordText();
        }
        return null;
    }
    public int getEndCodeIndex(){
        if(this.word!=null){
            return this.word.end;
        }else if(this.childrens.size()>0){
            return childrens.get(childrens.size()-1).getEndCodeIndex();
        }
        return -1;
    }
    public int getFirstCodeIndex(){
        if(this.word!=null){
            return this.word.start;
        }else if(this.childrens.size()>0){
            return childrens.get(0).getFirstCodeIndex();
        }
        return -1;
    }
    public String getEndWordText() {
        if(this.word!=null){
            return this.word.word;
        }else if(this.childrens.size()>0){
            return childrens.get(childrens.size()-1).getEndWordText();
        }
        return null;
    }

    @Override
    public String toString() {
        if(word!=null){
            return word.word+" ";
        }
        StringBuilder builder=new StringBuilder();
        for (CodeNode children : childrens) {
            builder.append(children);
        }
        return builder.toString();
    }
}
