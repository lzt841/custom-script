package com.lzt841.script.environment;

import com.lzt841.script.language.ls.model.ScriptFile;

import java.util.List;

public interface EnvironmentHandler {

//    List<CodeProblem> check(String scriptName);
//
//    List<CodePrompt> prompt(String scriptName,int index);

    Object execute(String scriptName,String funName,List parameters);
    Object execute(ScriptFile scriptFile, String funName, List parameters);
}
