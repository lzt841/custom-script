package com.lzt841.script.language.ls.lexical;

import com.lzt841.script.language.ls.model.Keyword;
import com.lzt841.script.lexical.StringCharHandler;

public class LsStringCharHandler extends StringCharHandler {
    public LsStringCharHandler(Keyword keyword) {
        this(keyword,keyword,keyword.token);
    }
    public LsStringCharHandler(Keyword startKeyword, Keyword endKeyword) {
        this(startKeyword, endKeyword,false, startKeyword.token);
    }
    public LsStringCharHandler(Keyword startKeyword, Keyword endKeyword, int token) {
        this(startKeyword, endKeyword,false, token);
    }
    public LsStringCharHandler(Keyword startKeyword, Keyword endKeyword, boolean lose, int token) {
        super(startKeyword.keyword, endKeyword.keyword,lose, token);
    }
}
