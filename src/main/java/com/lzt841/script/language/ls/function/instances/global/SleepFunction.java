package com.lzt841.script.language.ls.function.instances.global;

import com.lzt841.script.language.ls.function.FunctionHandler;
import com.lzt841.script.language.ls.utils.CalculateUtils;

import java.util.List;

public class SleepFunction implements FunctionHandler {
    @Override
    public Object execute(List parameters) {
        for (Object parameter : parameters) {
            Integer integer = CalculateUtils.toInteger(parameter);
            try {
                Thread.sleep(integer);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        return null;
    }
}
