package com.lzt841.script.language.ls.function.instances.variable;

import com.lzt841.script.language.ls.function.VariableFunction;
import com.lzt841.script.language.ls.utils.CalculateUtils;

import java.util.List;
import java.util.Map;

public class VariableGetFunction implements VariableFunction {
    @Override
    public Object execute(Object variable, List parameters) {

        if(parameters.size()==0)return null;
        Object key=parameters.get(0);
        if (Map.class.isInstance(variable)) {
            return ((Map) variable).get(key);
        }else if (List.class.isInstance(variable)) {
            return ((List) variable).get(CalculateUtils.toInteger(key));
        }else if(CharSequence.class.isInstance(variable)){
            return ((CharSequence) variable).charAt(CalculateUtils.toInteger(key))+"";
        }
        return null;
    }
}
