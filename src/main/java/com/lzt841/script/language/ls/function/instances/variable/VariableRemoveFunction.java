package com.lzt841.script.language.ls.function.instances.variable;

import com.lzt841.script.language.ls.function.VariableFunction;
import com.lzt841.script.language.ls.utils.CalculateUtils;

import java.util.List;
import java.util.Map;

public class VariableRemoveFunction implements VariableFunction {
    @Override
    public Object execute(Object variable, List parameters) {
        if(parameters.size()==0)return null;
        Object key=parameters.get(0);
        if (Map.class.isInstance(variable)) {
            return ((Map) variable).remove(key);
        }else if (List.class.isInstance(variable)) {
            return ((List) variable).remove(CalculateUtils.toInteger(key));
        }
        return null;
    }
}
