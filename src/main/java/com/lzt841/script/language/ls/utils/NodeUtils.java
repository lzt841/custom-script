package com.lzt841.script.language.ls.utils;

import com.lzt841.script.grammar.CodeNode;

import java.util.List;

public class NodeUtils {
    public static int findNodeByToken(List<CodeNode> nodeList, int token, int start){
        int size = nodeList.size();
        for (int i = start; i < size; i++) {
            if(token==(nodeList.get(i).getFirstWordToken()))
                return i;
        }
        return -1;
    }

}
