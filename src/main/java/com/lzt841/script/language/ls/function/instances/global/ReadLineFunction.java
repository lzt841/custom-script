package com.lzt841.script.language.ls.function.instances.global;

import com.lzt841.script.language.ls.function.FunctionHandler;

import java.util.List;
import java.util.Scanner;

public class ReadLineFunction implements FunctionHandler {
    Scanner scanner;
    public ReadLineFunction() {
        scanner=new Scanner(System.in);
    }
    @Override
    public Object execute(List parameters) {
        return scanner.nextLine();
    }
}
