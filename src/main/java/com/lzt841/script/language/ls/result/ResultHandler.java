package com.lzt841.script.language.ls.result;

import com.lzt841.script.grammar.CodeNode;
import com.lzt841.script.language.ls.LsExecuteContext;
import com.lzt841.script.language.ls.model.Result;
import com.lzt841.script.language.ls.model.ScriptFile;

import java.util.List;
import java.util.Map;

public interface ResultHandler {

    int calculate(LsExecuteContext context, ScriptFile scriptFile, Map<String, Object> variableMap, List<CodeNode> codeNodes, int start, Result result);

    int skipCalculate(LsExecuteContext context, List<CodeNode> codeNodes, int start);


}
