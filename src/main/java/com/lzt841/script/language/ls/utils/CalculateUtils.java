package com.lzt841.script.language.ls.utils;

import com.lzt841.script.grammar.CodeNode;
import com.lzt841.script.language.ls.LsExecuteContext;
import com.lzt841.script.language.ls.exception.FunctionNoFoundException;
import com.lzt841.script.language.ls.exception.LsRuntimeException;
import com.lzt841.script.language.ls.exception.VariableNoFoundException;
import com.lzt841.script.language.ls.function.VariableFunction;
import com.lzt841.script.language.ls.model.Result;
import com.lzt841.script.language.ls.model.ScriptFile;
import com.lzt841.script.language.ls.model.Variable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CalculateUtils {

    public static Object add(Object value1,Object value2){
        if(value1 instanceof CharSequence||value2 instanceof CharSequence){
            return ""+value1+value2;
        } else if (isInteger(value1)&& isInteger(value2)) {
            return (Integer) value1+(Integer) value2;
        }else if ((isLong(value1)||isInteger(value1))&&((isLong(value2)||isInteger(value2)))) {
            return toLong(value1)+toLong(value2);
        } else if(!isDouble(value1)&&!isDouble(value2)){
            return toFloat(value1)+toFloat(value2);
        }else {
            return toDouble(value1)+toDouble(value2);
        }
    }
    public static Object subtract(Object value1,Object value2){
        if(value1 instanceof CharSequence) {
            value1=stringToNumber(value1);
        }
        if(value2 instanceof CharSequence) {
            value2=stringToNumber(value2);
        }
        if (isInteger(value1)&& isInteger(value2)) {
            return (Integer) value1-(Integer) value2;
        }else if ((isLong(value1)||isInteger(value1))&&((isLong(value2)||isInteger(value2)))) {
            return toLong(value1)-toLong(value2);
        } else if(!isDouble(value1)&&!isDouble(value2)){
            return toFloat(value1)-toFloat(value2);
        }else {
            return toDouble(value1)-toDouble(value2);
        }
    }
    public static Object multiply(Object value1,Object value2){
        if(value1 instanceof CharSequence) {
            value1=stringToNumber(value1);
        }
        if(value2 instanceof CharSequence) {
            value2=stringToNumber(value2);
        }
        if (isInteger(value1)&& isInteger(value2)) {
            return (Integer) value1*(Integer) value2;
        }else if ((isLong(value1)||isInteger(value1))&&((isLong(value2)||isInteger(value2)))) {
            return toLong(value1)*toLong(value2);
        } else if(!isDouble(value1)&&!isDouble(value2)){
            return toFloat(value1)*toFloat(value2);
        }else {
            return toDouble(value1)*toDouble(value2);
        }
    }
    public static Object divide(Object value1,Object value2){
        if(value1 instanceof CharSequence) {
            value1=stringToNumber(value1);
        }
        if(value2 instanceof CharSequence) {
            value2=stringToNumber(value2);
        }
        if (isInteger(value1)&& isInteger(value2)) {
            return (Integer) value1/(Integer) value2;
        }else if ((isLong(value1)||isInteger(value1))&&((isLong(value2)||isInteger(value2)))) {
            return toLong(value1)/toLong(value2);
        } else if(!isDouble(value1)&&!isDouble(value2)){
            return toFloat(value1)/toFloat(value2);
        }else {
            return toDouble(value1)/toDouble(value2);
        }
    }
    public static boolean greater(Object value1,Object value2){
        if (isDouble(value1) || isDouble(value2)) {
            return toDouble(value1)>toDouble(value2);
        } else if (isFloat(value1) || isFloat(value2)) {
            return toFloat(value1)>toFloat(value2);
        }else if (isLong(value1) || isLong(value2)) {
            return toLong(value1)>toLong(value2);
        }else {
            return toInteger(value1)>toInteger(value2);
        }
    }
    public static boolean greaterOrEquals(Object value1,Object value2){
        if (isDouble(value1) || isDouble(value2)) {
            return toDouble(value1)>=toDouble(value2);
        } else if (isFloat(value1) || isFloat(value2)) {
            return toFloat(value1)>=toFloat(value2);
        }else if (isLong(value1) || isLong(value2)) {
            return toLong(value1)>=toLong(value2);
        }else {
            return toInteger(value1)>=toInteger(value2);
        }
    }
    public static boolean less(Object value1,Object value2){
        if (isDouble(value1) || isDouble(value2)) {
            return toDouble(value1)<toDouble(value2);
        } else if (isFloat(value1) || isFloat(value2)) {
            return toFloat(value1)<toFloat(value2);
        }else if (isLong(value1) || isLong(value2)) {
            return toLong(value1)<toLong(value2);
        }else {
            return toInteger(value1)<toInteger(value2);
        }
    }
    public static boolean lessOrEquals(Object value1,Object value2){
        if (isDouble(value1) || isDouble(value2)) {
            return toDouble(value1)<=toDouble(value2);
        } else if (isFloat(value1) || isFloat(value2)) {
            return toFloat(value1)<=toFloat(value2);
        }else if (isLong(value1) || isLong(value2)) {
            return toLong(value1)<=toLong(value2);
        }else {
            return toInteger(value1)<=toInteger(value2);
        }
    }
    public static boolean equals(Object value1,Object value2){
        if (isNumber(value1) && isNumber(value2)) {
            if (isDouble(value1) || isDouble(value2)) {
                return toDouble(value1).equals(toDouble(value2));
            } else if (isFloat(value1) || isFloat(value2)) {
                return toFloat(value1).equals(toFloat(value2));
            }else  if (isLong(value1) || isLong(value2)) {
                return toLong(value1).equals(toLong(value2));
            }else {
                return toInteger(value1).equals(toInteger(value2));
            }
        }else {
           if(value1==null&&value2==null)return true;
           if(value1==null||value2==null)return false;
           return value1.equals(value2);
        }
    }
    public static boolean unEquals(Object value1,Object value2){
       return !equals(value1,value2);
    }

    public static Object stringToNumber(Object value){
        if(value==null||((CharSequence)value).length()==0)return 0;
        int l=((CharSequence) value).length();
        char c = ((CharSequence) value).charAt(l - 1);
        if(c=='f'||c=='F'){
            try{
                return Float.parseFloat(value.toString());
            }catch (Exception e){}
        }else if(c=='l'||c=='L'){
            try{
                return Long.parseLong(value.toString());
            }catch (Exception e){}
        }
        try{
            return Double.parseDouble(value.toString());
        }catch (Exception e){}
        try{
            return Integer.parseInt(value.toString());
        }catch (Exception e){}
        return 0;
    }
    public static Double toDouble(Object value){
        if(Double.class.isInstance(value))return ((Double) value);
        if(Float.class.isInstance(value))return ((Float) value).doubleValue();
        if(Integer.class.isInstance(value))return ((Integer) value).doubleValue();
        if(Long.class.isInstance(value))return ((Long) value).doubleValue();
        if(Boolean.class.isInstance(value))return ((Boolean) value)?1.0:0;
        return 0.0;
    }
    public static Long toLong(Object value){
        if(Long.class.isInstance(value))return ((Long) value);
        if(Integer.class.isInstance(value))return ((Integer) value).longValue();
        if(Float.class.isInstance(value))return ((Float) value).longValue();
        if(Double.class.isInstance(value))return ((Double) value).longValue();
        if(Boolean.class.isInstance(value))return ((Boolean) value)?1L:0L;
        return 0L;
    }
    public static Float toFloat(Object value){
        if(Float.class.isInstance(value))return (Float) value;
        if(Integer.class.isInstance(value))return ((Integer) value).floatValue();
        if(Long.class.isInstance(value))return ((Long) value).floatValue();
        if(Double.class.isInstance(value))return ((Double) value).floatValue();
        if(Boolean.class.isInstance(value))return ((Boolean) value)?1f:0;
        return 0f;
    }
    public static Integer toInteger(Object value){
        if(Integer.class.isInstance(value))return (Integer) value;
        if(Float.class.isInstance(value))return ((Float) value).intValue();
        if(Long.class.isInstance(value))return ((Long) value).intValue();
        if(Double.class.isInstance(value))return ((Double) value).intValue();
        if(Boolean.class.isInstance(value))return ((Boolean) value)?1:0;
        return 0;
    }
    public static boolean toBoolean(Object value) {
        if(Boolean.class.isInstance(value))return ((Boolean) value);
        if(Float.class.isInstance(value))return (Float) value>0;
        if(Integer.class.isInstance(value))return ((Integer) value)>0;
        if(Long.class.isInstance(value))return ((Long) value)>0;
        if(Double.class.isInstance(value))return ((Double) value)>0;
        return false;
    }
    public static boolean isInteger(Object value){
        return Integer.class.isInstance(value);
    }
    public static boolean isDouble(Object value){
        return Double.class.isInstance(value);
    }
    public static boolean isFloat(Object value){
        return Float.class.isInstance(value);
    }
    public static boolean isLong(Object value){
        return Long.class.isInstance(value);
    }
    public static boolean isBoolean(Object value){
        return Boolean.class.isInstance(value);
    }
    public static boolean isTrue(Object value){
        if(value==null)return false;
        if(Boolean.class.isInstance(value))return (Boolean) value;
        if(Integer.class.isInstance(value))return (Integer)(value)>0;
        if(Float.class.isInstance(value)) return (Float)(value)>0;
        if(Double.class.isInstance(value)) return (Double)(value)>0;
        if(Long.class.isInstance(value)) return ((Long)value)>0;
        if(CharSequence.class.isInstance(value)) {
            return ((CharSequence) value).length()>0;
        }
        return true;
    }
    public static boolean isNumber(Object value){
        if(Integer.class.isInstance(value))return true;
        if(Float.class.isInstance(value)) return true;
        if(Double.class.isInstance(value)) return true;
        if(Long.class.isInstance(value)) return true;
        return false;
    }

    /**
     * 获取方法参数值
     * @param executeHandle
     * @param scriptFile
     * @param variableMap
     * @param childrens
     * @return
     */
    public static List getFunctionParameters(LsExecuteContext executeHandle, ScriptFile scriptFile, Map<String, Object> variableMap, List<CodeNode> childrens) {
        Result result=new Result();
        List list=new ArrayList<>();
        for (int i = 1; i < childrens.size()-1; i+=2) {
            result.result=null;
            i=executeHandle.getResult(scriptFile,childrens,variableMap,i,result);
            list.add(result.result);
        }
        return list;
    }
    /**
     * 找到操作变量
     * @param executeHandle
     * @param scriptFile
     * @param variableMap
     * @param codeNodes
     * @param i
     * @return
     */
    public static Variable findVariableLeft(LsExecuteContext executeHandle, ScriptFile scriptFile, Map<String, Object> variableMap, List<CodeNode> codeNodes, int i) {
        List names=new ArrayList<>();
        int up=0;// a=b["xx"][""] b.c="" b[c]["11"]=""
        int type= Variable.DEFAULT;
        for (; i>=0 ; i--) {
            CodeNode node = codeNodes.get(i);
            int token=node.getFirstWordToken();
            String firstWordText=node.getFirstWordText();
            if(executeHandle.keyWordConfig.SUBSET_LINK_KEYWORD.token==token){
                if(up!=2)break;
                up=3;
            }else if(executeHandle.keyWordConfig.SUBSET_START_KEYWORD.token==token){
                if(up!=0&&up!=1)break;
                Result result=new Result();
                executeHandle.getResult(scriptFile,node.childrens,variableMap,1,result);
                names.add(0,result.result);
                up=1;
            }else {
                if(up==2)break;
                names.add(0, firstWordText);
                up=2;
                if(token== executeHandle.keyWordConfig.GLOBAL_KEYWORD.token){
                    type= Variable.GLOBAL;
                }else if(token== executeHandle.keyWordConfig.SELF_KEYWORD.token){
                    type= Variable.SELF;
                }
            }
        }

        return findVariableLeft(names,executeHandle,scriptFile,variableMap,codeNodes,i+1,type);

    }
    private static Variable findVariableLeft(List names, LsExecuteContext context, ScriptFile scriptFile, Map<String, Object> variableMap, List<CodeNode> codeNodes, int i, int type) {
        String last=names.get(0).toString();
        Variable variable=new Variable();
        Object library=null;
        if(type== Variable.GLOBAL){
            variable.type= Variable.GLOBAL;
            library=context.getGlobalVariable();
            names.remove(0);
        } else if(type== Variable.SELF){
            variable.type= Variable.SELF;
            library=scriptFile.variableMap;
            names.remove(0);
        } else if(variableMap.containsKey(last)){
            library=variableMap;
            variable.type= Variable.DEFAULT;
        }else if(scriptFile.variableMap.containsKey(last)){
            library=scriptFile.variableMap;
            variable.type= Variable.SELF;
        } else if (context.hasGlobalVariable(last)) {
            library=context.getGlobalVariable();
            variable.type= Variable.GLOBAL;
        }else {
            library=variableMap;
        }

        variable.library=library;
        int size = names.size()-1;
        for (int j = 0; j < size; j++) {
            variable.key=names.get(j);
            if(context.getVariableOperation().isExistence(variable)){
                variable.library=context.getVariableOperation().getVariableValue(variable);
            }else {
                LsRuntimeException.CodeErrorDetail detail=new LsRuntimeException.CodeErrorDetail();
                detail.message="变量 '"+names.get(j)+"' 不存在或未初始化!请先初始化再操作!";
                detail.codeStartIndex=codeNodes.get(i-j).getFirstCodeIndex();
                detail.codeEndIndex=codeNodes.get(i-j).getEndCodeIndex();
                throw new VariableNoFoundException(detail);
            }
        }
        variable.key=names.get(names.size()-1);
        variable.end=i;
        return variable;
    }
    /**
     * 找到操作变量
     * @param context
     * @param scriptFile
     * @param variableMap
     * @param codeNodes
     * @param i
     * @return
     */
    public static Variable findVariableRight(LsExecuteContext context, ScriptFile scriptFile, Map<String, Object> variableMap, List<CodeNode> codeNodes, int i) {

        int up=0;//0: null ,1: [ ,2: name ,3: . 4: (
        int length=codeNodes.size();
        int type=Variable.DEFAULT;
        Variable variable=null;
        for (; i<length ; i++) {
            CodeNode node = codeNodes.get(i);
            String firstWordText = node.getFirstWordText();
            int token=node.getFirstWordToken();
            if(context.keyWordConfig.SUBSET_LINK_KEYWORD.token==token){
                if(up!=2&&up!=1&&up!=4)break;
                up=3;//.
            }else if(context.keyWordConfig.SUBSET_START_KEYWORD.token==token){
                if(up!=2&&up!=1&&up!=4)break;
                up=1;//[
                Result result=new Result();
                context.getResult(scriptFile,node.childrens,variableMap,1,result);
                variable.library=context.getVariableOperation().getVariableValue(variable);
                variable.key=result.result;
                variable.noKey=false;
            }else {
                if(up==2||up==4||up==1)break;
                if(up==3&&i+1<length&&context.keyWordConfig.PARAMETER_START_KEYWORD.token==codeNodes.get(i+1).getFirstWordToken()){
                    List functionParameters = getFunctionParameters(context, scriptFile, variableMap, codeNodes.get(i + 1).childrens);
                    Result result ;
                    if(variable==null||(type!=Variable.DEFAULT&&variable.noKey&&(variable.library==context.getGlobalVariable()||variable.library==scriptFile.variableMap))) {
                       Object fun;
                        if(variable==null&& ScriptFile.Function.class.isInstance(fun=variableMap.get(firstWordText))){
                            result = context.executeScriptFun((ScriptFile.Function) fun, getFunctionParameters(context, scriptFile, variableMap, ((ScriptFile.Function) fun).parameterNode.childrens));
                        }else {
                           result = context.executeFun(codeNodes, i, firstWordText, functionParameters, scriptFile, type);
                       }
                    } else {
                        Object variableValue = context.getVariableOperation().getVariableValue(variable);
                        variable.noKey=true;
                        variable.library=variableValue;
                        variable.key=firstWordText;
                        VariableFunction variableFunction= context.getVariableFunction(firstWordText);
                        Object value = null;
                        if(variableFunction==null||((value=context.getVariableOperation().getVariableValue(variable))!=null&& ScriptFile.Function.class.isInstance(value))) {
                            if(value==null){
                                LsRuntimeException.CodeErrorDetail detail=new LsRuntimeException.CodeErrorDetail();
                                detail.message="变量 '"+firstWordText+"' 不是方法!";
                                detail.codeStartIndex=node.getFirstCodeIndex();
                                detail.codeEndIndex=node.getEndCodeIndex();
                                throw new FunctionNoFoundException(detail);
                            }
                            else {
                                result=context.executeScriptFun(((ScriptFile.Function) value),getFunctionParameters(context,scriptFile,variableMap,((ScriptFile.Function)value).parameterNode.childrens));
                            }
                        }else {
                            result = context.executeVariableFun(codeNodes,i,variableValue,firstWordText, functionParameters);
                        }
                    }
                    variable.noKey=true;
                    variable.library=result.result;
                    variable.key=null;
                    i++;
                    up=4;
                }else if(up==0&&i+1<length&&context.keyWordConfig.PARAMETER_START_KEYWORD.token==codeNodes.get(i+1).getFirstWordToken()) {
                    List functionParameters = getFunctionParameters(context, scriptFile, variableMap, codeNodes.get(i + 1).childrens);
                    Result result ;
                    Object fun;
                    if(type == Variable.DEFAULT && (fun = variableMap.get(firstWordText)) != null && ScriptFile.Function.class.isInstance(fun)) {
                        result = context.executeScriptFun((ScriptFile.Function) fun,functionParameters);
                    }else {
                        result = context.executeFun(codeNodes, i, firstWordText, functionParameters, scriptFile, type);
                    }
                    variable=new Variable();
                    variable.noKey=true;
                    variable.library=result.result;
                    variable.key=null;
                    i++;
                    up=4;
                }else{
                    if(token==context.keyWordConfig.GLOBAL_KEYWORD.token&&variable==null){
                            variable=new Variable();
                            variable.noKey=true;
                            variable.library=context.getGlobalVariable();
                            type=Variable.GLOBAL;
                            variable.key=null;
                        }else if(token==context.keyWordConfig.SELF_KEYWORD.token&&variable==null){
                            variable=new Variable();
                            variable.noKey=true;
                            variable.library=scriptFile.variableMap;
                            type=Variable.SELF;
                            variable.key=null;
                        }else if(variable==null){
                            variable=new Variable();
                            variable.noKey=false;
                            if(variableMap.containsKey(firstWordText)) {
                                variable.library = variableMap;
                            } else if (scriptFile.variableMap.containsKey(firstWordText)) {
                                variable.library=scriptFile.variableMap;
                            }else if(context.getGlobalVariable().containsKey(firstWordText)){
                                variable.library=context.getGlobalVariable();
                            }else {
                                variable.library=variableMap;
                            }
                            variable.key=firstWordText;
                        }else {
                            variable.library = context.getVariableOperation().getVariableValue(variable);
                            variable.key = firstWordText;
                            variable.noKey = false;
                        }
                        up=2;
                    }
                }
            }

        variable.end=i-1;
        return variable;
    }



    public static int findVariableRightIndex(LsExecuteContext context,List<CodeNode> codeNodes, int i) {
        int up=0;//0: null ,1: [ ,2: name ,3: . 4: (
        int length=codeNodes.size();
        for (; i<length ; i++) {
            CodeNode node = codeNodes.get(i);
            int token=node.getFirstWordToken();
            if(context.keyWordConfig.SUBSET_LINK_KEYWORD.token==token){//.
                if(up!=2&&up!=1&&up!=4)break;
                up=3;//.
            }else if(context.keyWordConfig.SUBSET_START_KEYWORD.token==token){//[
                if(up!=2&&up!=1&&up!=4)break;
                up=1;//[
            }else if(context.keyWordConfig.PARAMETER_START_KEYWORD.token==token) {//(
                if(up!=2)break;
                up=4;
            } else {
                if(up==2||up==4||up==1)break;
                up=2;
            }
        }
        return i-1;
    }

}
