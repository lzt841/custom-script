package com.lzt841.script.language.ls.function.instances.global;

import com.lzt841.script.language.ls.function.FunctionHandler;

import java.io.IOException;
import java.util.List;

public class InputFunction implements FunctionHandler {
    @Override
    public Object execute(List parameters) {
        try {
            return System.in.read();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
