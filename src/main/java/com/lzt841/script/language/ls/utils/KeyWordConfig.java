package com.lzt841.script.language.ls.utils;

import com.lzt841.script.language.ls.model.Keyword;

public class KeyWordConfig {
    public  final Keyword FUNCTION_KEYWORD=new Keyword("function",113);
    public  final Keyword GLOBAL_KEYWORD=new Keyword("global",114);
    public  final Keyword SELF_KEYWORD=new Keyword("self",115);
    public  final Keyword IF_KEYWORD=new Keyword("if",116);
    public  final Keyword ELSE_KEYWORD=new Keyword("else",117);
    public  final Keyword WHILE_KEYWORD=new Keyword("while",118);
    public  final Keyword FOR_KEYWORD=new Keyword("for",119);
    public  final Keyword DO_KEYWORD=new Keyword("do",120);
    public  final Keyword ARRAY_LENGTH_KEYWORD=new Keyword("length",121);
    public  final Keyword FALSE_KEYWORD=new Keyword("false",122);
    public  final Keyword TRUE_KEYWORD=new Keyword("true",123);
    public  final Keyword NULL_KEYWORD=new Keyword("null",124);
    public  final Keyword RETURN_KEYWORD=new Keyword("return",125);
    public  final Keyword BREAK_KEYWORD=new Keyword("break",126);
    public  final Keyword CONTINUE_KEYWORD=new Keyword("continue",127);

    public  final Keyword PARTITION_KEYWORD=new Keyword(":",128);
    public  final Keyword INTERVAL_KEYWORD=new Keyword(",",129);
    public  final Keyword SEPARATE_KEYWORD=new Keyword(";",130);
    public  final Keyword PARAMETER_START_KEYWORD=new Keyword("(",131);
    public  final Keyword PARAMETER_END_KEYWORD=new Keyword(")",132);
    public  final Keyword EXECUTOR_START_KEYWORD=new Keyword("{",133);
    public  final Keyword EXECUTOR_END_KEYWORD=new Keyword("}",134);
    public  final Keyword OBJECT_START_KEYWORD=new Keyword("{",133);
    public  final Keyword OBJECT_END_KEYWORD=new Keyword("}",134);
    public  final Keyword ARRAY_START_KEYWORD=new Keyword("[",137);
    public  final Keyword ARRAY_END_KEYWORD=new Keyword("]",138);
    public  final Keyword SUBSET_START_KEYWORD=new Keyword("[",137);
    public  final Keyword SUBSET_END_KEYWORD=new Keyword("]",138);
    public  final Keyword ANNOTATION_START_KEYWORD=new Keyword("/*",139);
    public  final Keyword ANNOTATION_END_KEYWORD=new Keyword("*/",140);
    public  final Keyword ANNOTATION_KEYWORD=new Keyword("//",141);
    public  final Keyword STRING2_START_KEYWORD=new Keyword("\"",142);
    public  final Keyword STRING2_END_KEYWORD=new Keyword("\"",142);
    public  final Keyword STRING1_START_KEYWORD=new Keyword("'",144);
    public  final Keyword STRING1_END_KEYWORD=new Keyword("'",144);
    public  final Keyword SUBSET_LINK_KEYWORD=new Keyword(".",146);
    public  final Keyword FUNCTION_ARROW_KEYWORD=new Keyword("=>",147);
    public  final Keyword ENTER_KEYWORD=new Keyword("\n",149);
    public  final Keyword NUMBER_KEYWORD=new Keyword(null,150);
    public  final Keyword DEFAULT_KEYWORD=new Keyword("",-1);

}
