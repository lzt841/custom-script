package com.lzt841.script.language.ls.function.instances.variable;

import com.lzt841.script.language.ls.function.VariableFunction;
import com.lzt841.script.language.ls.utils.CalculateUtils;

import java.util.Arrays;
import java.util.List;

public class VariableSplitStringFunction implements VariableFunction {
    @Override
    public Object execute(Object variable, List parameters) {
        if(!CharSequence.class.isInstance(variable)||parameters.size()==0)return null;
        if(parameters.size()==1){
            return Arrays.asList(variable.toString().split(parameters.get(0)+""));
        }
        return Arrays.asList(variable.toString().split(parameters.get(0)+"", CalculateUtils.toInteger(parameters.get(1))));
    }
}
