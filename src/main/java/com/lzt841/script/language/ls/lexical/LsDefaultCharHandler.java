package com.lzt841.script.language.ls.lexical;

import com.lzt841.script.language.ls.model.Keyword;
import com.lzt841.script.lexical.DefaultCharHandler;
import com.lzt841.script.lexical.Word;

import java.util.List;

public class LsDefaultCharHandler extends DefaultCharHandler {
    int token;

    public LsDefaultCharHandler(Keyword keyword){
        this(keyword.token);
    }
    public LsDefaultCharHandler(int token) {
        this.token = token;
    }

    @Override
    public void exit(CharSequence text, int index, List<Word> wordsOut) {
        Word w=new Word();
        w.word=text.subSequence(start,index).toString();
        w.start=start;
        w.token=token;
        w.end=index;
        wordsOut.add(w);
    }
}
