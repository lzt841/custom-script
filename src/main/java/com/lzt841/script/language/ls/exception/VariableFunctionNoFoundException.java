package com.lzt841.script.language.ls.exception;

public class VariableFunctionNoFoundException extends LsRuntimeException {
    public static final int ERROR_CODE=2;
    public VariableFunctionNoFoundException(CodeErrorDetail detail) {
        super(detail);
        detail.errorCode=ERROR_CODE;
    }
}
