package com.lzt841.script.language.ls.function.instances.variable;

import com.lzt841.script.language.ls.function.VariableFunction;
import com.lzt841.script.language.ls.utils.CalculateUtils;

import java.util.List;
import java.util.Map;

public class VariableSetFunction implements VariableFunction {
    @Override
    public Object execute(Object variable, List parameters) {
        if(parameters.size()<2)return null;
        Object key=parameters.get(0);
        if (Map.class.isInstance(variable)) {
            return ((Map) variable).put(key,parameters.get(1));
        }else if (List.class.isInstance(variable)) {
            return ((List) variable).set(CalculateUtils.toInteger(key),parameters.get(1));
        }
        return null;
    }
}
