package com.lzt841.script.language.ls.function.instances.variable;

import com.lzt841.script.language.ls.function.VariableFunction;
import com.lzt841.script.language.ls.utils.CalculateUtils;

import java.util.List;
import java.util.Map;

public class VariableHasFunction implements VariableFunction {
    @Override
    public Object execute(Object variable, List parameters) {
        if (List.class.isInstance(variable)) {
           return listHas((List)variable,parameters);
        } else if (Map.class.isInstance(variable)) {
            return mapHas((Map)variable,parameters);
        }
        return false;
    }

    private Object mapHas(Map variable, List parameters) {
        for (Object parameter : parameters) {
          if(!variable.containsKey(parameter)){
              return false;
          }
        }
        return true;
    }

    private Object listHas(List variable, List parameters) {
        int size=variable.size();
        for (Object parameter : parameters) {
            if(CalculateUtils.isInteger(parameter)){
                Integer index= (Integer) parameter;
                if(index<0||index>=size)return false;
            }else {
                return false;
            }
        }
        return true;
    }
}
