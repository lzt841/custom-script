package com.lzt841.script.language.ls.function.instances.global;

import com.lzt841.script.language.ls.function.FunctionHandler;

import java.util.List;

public class TimeFunction implements FunctionHandler {
    @Override
    public Object execute(List parameters) {
        return System.currentTimeMillis();
    }
}
