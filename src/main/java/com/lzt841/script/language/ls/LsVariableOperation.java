package com.lzt841.script.language.ls;

import com.lzt841.script.language.ls.model.Variable;
import com.lzt841.script.language.ls.utils.CalculateUtils;

import java.util.List;
import java.util.Map;

public class LsVariableOperation implements Variable.VariableOperation {
    public LsExecuteContext context;

    public LsVariableOperation(LsExecuteContext context) {
        this.context = context;
    }

    @Override
    public void setVariableValue(Variable variable, Object value){
        if(variable.noKey){
            variable.library=value;
        } else if(variable.library instanceof Map)
            ((Map) variable.library).put(variable.key,value);
        else if(variable.library instanceof List){
            List list= (List) variable.library;
            Integer integer = CalculateUtils.toInteger(variable.key);
            if(integer<list.size())
                list.set(integer,value);
            else
                list.add(value);
        }else if(CharSequence.class.isInstance(variable.library)){
            //字符串

        }else if(variable.library==null){
            //变量未初始化
        }else {

        }
    }
    @Override
    public Object getVariableValue(Variable variable){
        if(variable.noKey)return variable.library;
        if(variable.library instanceof Map)
            return  ((Map) variable.library).get(variable.key);
        else if(variable.library instanceof List){
            List list= (List) variable.library;
            if(context.keyWordConfig.ARRAY_LENGTH_KEYWORD.keyword.equals(variable.key)){
                return list.size();
            }
            return list.get(CalculateUtils.toInteger(variable.key));
        }else if(CharSequence.class.isInstance(variable.library)){
            return ((CharSequence) variable.library).charAt(CalculateUtils.toInteger(variable.key));
        }
        return null;
    }
    @Override
    public boolean isExistence(Variable variable){
            if(variable.library instanceof Map) {
                return ((Map) variable.library).containsKey(variable.key);
            }
            else if(variable.library instanceof List){
                List list= (List) variable.library;
                try{
                    int i = CalculateUtils.toInteger(variable.key);
                    return i>=0&&i<list.size();
                }
                catch (Exception e){
                    return false;
                }
            }else {
                return variable.key==null&&variable.library!=null;
            }

    }
}
