package com.lzt841.script.language.ls.function.instances.global;

import com.lzt841.script.language.ls.function.FunctionHandler;

import java.util.List;

public class PrintfFunction implements FunctionHandler {

    @Override
    public Object execute(List parameters) {
        for (Object parameter : parameters) {
            System.out.print(parameter);
        }
       return null;
    }
}
