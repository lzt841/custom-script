package com.lzt841.script.language.ls.function.instances.variable;

import com.lzt841.script.language.ls.function.VariableFunction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VariableConcatFunction implements VariableFunction {
    //合并
    @Override
    public Object execute(Object variable, List parameters) {
        if(List.class.isInstance(variable)){
            List newList= new ArrayList();
            Collections.copy(newList,(List)variable);
            for (Object parameter : parameters) {
                if(List.class.isInstance(parameter)){
                    newList.addAll((List) parameter);
                }else
                    newList.add(parameter);
            }
            return newList;
        } else if (Map.class.isInstance(variable)) {
            Map newMap=new HashMap<>();
            for (Object o : ((Map)variable).keySet()) {
                newMap.put(o,((Map)variable).get(o));
            }
            for (Object parameter : parameters) {
                if(Map.class.isInstance(parameter)){
                    newMap.putAll((Map) parameter);
                }
            }
            return newMap;
        } else if (CharSequence.class.isInstance(variable)) {
            for (Object parameter : parameters) {
                variable+=parameter+"";
            }
            return variable;
        }
        return null;
    }
}
