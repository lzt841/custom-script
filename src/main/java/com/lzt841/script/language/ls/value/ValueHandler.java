package com.lzt841.script.language.ls.value;

import com.lzt841.script.grammar.CodeNode;
import com.lzt841.script.language.ls.LsExecuteContext;
import com.lzt841.script.language.ls.model.ScriptFile;
import com.lzt841.script.language.ls.model.Value;

import java.util.List;
import java.util.Map;

public interface ValueHandler {
    boolean canGetValue(LsExecuteContext context, List<CodeNode> codeNodes, int start);

    Value getValue(LsExecuteContext context, ScriptFile scriptFile, Map<String, Object> variableMap, List<CodeNode> codeNodes, int start);

    int skipValue(LsExecuteContext context, List<CodeNode> codeNodes, int start);
}
