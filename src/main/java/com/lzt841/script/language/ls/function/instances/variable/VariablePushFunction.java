package com.lzt841.script.language.ls.function.instances.variable;

import com.lzt841.script.language.ls.function.VariableFunction;

import java.util.List;
import java.util.Map;

public class VariablePushFunction implements VariableFunction {
    @Override
    public Object execute(Object variable, List parameters) {

        if(parameters.size()==0)return null;
        if (Map.class.isInstance(variable)) {
            return pushMap((Map) variable,parameters);
        }else if (List.class.isInstance(variable)) {
            return pushList((List) variable,parameters);
        }else if(CharSequence.class.isInstance(variable)){
            for (Object parameter : parameters) {
                variable+=parameter+"";
            }
            return variable;
        }
        return null;
    }

    private Object pushMap(Map variable, List parameters) {
        return null;
    }

    private Object pushList(List variable, List parameters) {
        for (Object parameter : parameters) {
            variable.add(parameter);
        }
        if(parameters.size()==1)return parameters.get(0);
        return parameters;
    }
}
