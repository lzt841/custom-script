package com.lzt841.script.language.ls.execute;

import com.lzt841.script.grammar.CodeNode;
import com.lzt841.script.language.ls.LsExecuteContext;
import com.lzt841.script.language.ls.model.Result;
import com.lzt841.script.language.ls.model.ScriptFile;

import java.util.List;
import java.util.Map;

public interface ExecuteHandler {

    boolean canExecute(LsExecuteContext context, List<CodeNode> codeNodes, int i);

    int execute(LsExecuteContext context, ScriptFile scriptFile, Map<String,Object> variableMap, List<CodeNode> codeNodes, int i, Result funResult);
	int skipExecute(LsExecuteContext context,  List<CodeNode> codeNodes, int i);
	

}
