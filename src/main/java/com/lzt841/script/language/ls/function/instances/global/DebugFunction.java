package com.lzt841.script.language.ls.function.instances.global;

import com.lzt841.script.language.ls.LsEnvironment;
import com.lzt841.script.language.ls.function.FunctionHandler;

import java.util.List;

public class DebugFunction implements FunctionHandler {
    public LsEnvironment environment;

    public DebugFunction(LsEnvironment environment) {
        this.environment = environment;
    }

    @Override
    public Object execute(List parameters) {
        return null;
    }
}
