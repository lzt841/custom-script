package com.lzt841.script.language.ls.grammar;

import com.lzt841.script.grammar.GrammarTreeAnalysis;

import com.lzt841.script.grammar.NodeGroupHandler;
import com.lzt841.script.language.ls.LsExecuteContext;


public class LsGrammarTreeAnalysis extends GrammarTreeAnalysis {

    private final LsExecuteContext context;
    public LsGrammarTreeAnalysis(LsExecuteContext context) {
        this.context=context;
        init();
    }


    private void init() {
        addNodeGroupHandler(new LsNodeGroupHandler( context.keyWordConfig.EXECUTOR_START_KEYWORD.token, context.keyWordConfig.EXECUTOR_END_KEYWORD.token));
        addNodeGroupHandler(new LsNodeGroupHandler( context.keyWordConfig.PARAMETER_START_KEYWORD.token, context.keyWordConfig.PARAMETER_END_KEYWORD.token));
        addNodeGroupHandler(new LsNodeGroupHandler( context.keyWordConfig.ARRAY_START_KEYWORD.token, context.keyWordConfig.ARRAY_END_KEYWORD.token));
        addNodeGroupHandler(new LsNodeGroupHandler( context.keyWordConfig.OBJECT_START_KEYWORD.token, context.keyWordConfig.OBJECT_END_KEYWORD.token));
    }
}
