package com.lzt841.script.language.ls.exception;

public class FunctionNoFoundException extends LsRuntimeException {
    public static final int ERROR_CODE=3;
    public FunctionNoFoundException(CodeErrorDetail detail) {
        super(detail);
        detail.errorCode=ERROR_CODE;
    }
}
