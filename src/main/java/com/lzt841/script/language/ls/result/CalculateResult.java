package com.lzt841.script.language.ls.result;

import com.lzt841.script.grammar.CodeNode;
import com.lzt841.script.language.ls.LsExecuteContext;
import com.lzt841.script.language.ls.model.Result;
import com.lzt841.script.language.ls.model.ScriptFile;
import com.lzt841.script.language.ls.model.Value;
import com.lzt841.script.language.ls.utils.CalculateUtils;
import com.lzt841.script.language.ls.utils.NodeUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import com.lzt841.script.language.ls.exception.*;

public class CalculateResult implements ResultHandler, OperatorHandler {

    public List<OperatorValue> operatorValues=new ArrayList<>();

	

	public List<OperatorValue> getOperatorValues()
	{
		return operatorValues;
	}

    @Override
    public int calculate(LsExecuteContext context, ScriptFile scriptFile, Map<String, Object> variableMap, List<CodeNode> codeNodes, int start, Result result) {
        if(start<0||start>=codeNodes.size())return start;
        List<Value> results=new ArrayList<>();
        Value calculate= context.getValue(scriptFile,variableMap,codeNodes,start);
        results.add(calculate);
        calculateNext(context,scriptFile,variableMap,codeNodes,calculate.start,results);
        Value statisticsResult=statisticsResult(context,results);
        result.result=statisticsResult.value;
        return statisticsResult.start;
    }



    @Override
    public int skipCalculate(LsExecuteContext context, List<CodeNode> codeNodes, int start) {
        start= context.skipValue(codeNodes,start);
        start= skipNext(context,codeNodes,start);
        return start;
    }



    private void calculateNext(LsExecuteContext context, ScriptFile scriptFile, Map<String, Object> variableMap, List<CodeNode> codeNodes, int start, List<Value> values){
        if(start+1>=codeNodes.size())return;

        CodeNode node = codeNodes.get(start+1);
        int operatorToken=node.getFirstWordToken();

        for (OperatorValue operatorValue : operatorValues) {
            if(operatorValue.canOperator(context,operatorToken)){
                start=operatorValue.getOperatorValue(context,scriptFile,variableMap,codeNodes,start+1,values,operatorToken,this);
				calculateNext(context, scriptFile, variableMap, codeNodes, start, values);
                return;
            }
        }
        Value value;
        if (context.operatorSymbolConfig.OR2.token==operatorToken) {
            Value statisticsResult = statisticsResult(context,values);
            if(!CalculateUtils.isTrue(statisticsResult.value)) {//前面结果不为true,才执行下面结果
                start += 2;
                value = context.getValue( scriptFile, variableMap, codeNodes, start);
                value.operatorToken =operatorToken;
                values.add(value);
                calculateNext(context, scriptFile, variableMap, codeNodes, value.start, values);
            }else {
                statisticsResult.start =context.skipResult(codeNodes,statisticsResult.start +2);
            }
        } else if (context.operatorSymbolConfig.AND2.token==operatorToken) {
            Value statisticsResult = statisticsResult(context,values);
            if(CalculateUtils.isTrue(statisticsResult.value)) {//前面结果为true,才执行下面结果
                start += 2;
                value = context.getValue( scriptFile, variableMap, codeNodes, start);
                value.operatorToken =operatorToken;
                values.add(value);
                calculateNext(context, scriptFile, variableMap, codeNodes, value.start, values);
            }else {
                statisticsResult.start =context.skipResult(codeNodes,statisticsResult.start +2);
            }
        } else if (context.operatorSymbolConfig.DOUBT.token==operatorToken) {
            Value statisticsResult = statisticsResult(context,values);
            if(CalculateUtils.isTrue(statisticsResult.value)) {//前面结果为true,执行下面结果
                start += 2;
                value = context.getValue( scriptFile, variableMap, codeNodes, start);
                value.operatorToken =operatorToken;
                values.add(value);
                calculateNext(context, scriptFile, variableMap, codeNodes, value.start, values);
                statisticsResult = statisticsResult(context,values);
                statisticsResult.start =context.skipResult(codeNodes,statisticsResult.start +2);
                int a=statisticsResult.start;
            }else {//否则执行:后面结果
                start+=2;
                start= NodeUtils.findNodeByToken(codeNodes, context.keyWordConfig.PARTITION_KEYWORD.token,start)+1;
                if(start!=-1){
                    value = context.getValue( scriptFile, variableMap, codeNodes, start);
                    value.operatorToken =operatorToken;
                    values.add(value);
                    calculateNext(context, scriptFile, variableMap, codeNodes, value.start, values);
                }
            }
        } else if(getOperatorLevel(context,operatorToken)!= context.operatorSymbolConfig.DEFAULT.operatorLevel) {

                start += 2;
                value = context.getValue(scriptFile, variableMap, codeNodes, start);
                value.operatorToken = operatorToken;
                values.add(value);
                calculateNext(context, scriptFile, variableMap, codeNodes, value.start, values);
        }

    }



    private Object calculate(LsExecuteContext context,Value left, int operatorToken, Value right) {
        for (OperatorValue operatorValue : operatorValues) {
            if(operatorValue.canOperator(context,operatorToken)){
                return operatorValue.operator(context,left,operatorToken,right);
            }
        }
        Object v;
        if(context.operatorSymbolConfig.ASSIGNMENT.token==operatorToken){
            context.getVariableOperation().setVariableValue(left.variable,right.value);
            return right.value;
        } else if(context.operatorSymbolConfig.DOUBT.token==operatorToken) {
            if(left.variable!=null)
                context.getVariableOperation().setVariableValue(left.variable,right.value);
            return right.value;
        }
        else if(context.operatorSymbolConfig.RIGHT_MOVE2_ASSIGNMENT.token==operatorToken) {
            v = CalculateUtils.toLong(left.value)>>> CalculateUtils.toLong(right.value);
            context.getVariableOperation().setVariableValue(left.variable,v);
            return v;
        }
        else if(context.operatorSymbolConfig.RIGHT_MOVE_ASSIGNMENT.token==operatorToken) {
            v = CalculateUtils.toLong(left.value)>> CalculateUtils.toLong(right.value);
            context.getVariableOperation().setVariableValue(left.variable,v);
            return v;
        }
        else if(context.operatorSymbolConfig.LEFT_MOVE_ASSIGNMENT.token==operatorToken) {
            v = CalculateUtils.toLong(left.value) << CalculateUtils.toLong(right.value);
            context.getVariableOperation().setVariableValue(left.variable,v);
            return v;
        }
        else if(context.operatorSymbolConfig.OR_ASSIGNMENT.token==operatorToken){
            v = CalculateUtils.toLong(left.value)| CalculateUtils.toLong(right.value);
            context.getVariableOperation().setVariableValue(left.variable,v);
            return v;
        }
        else if(context.operatorSymbolConfig.AND_ASSIGNMENT.token==operatorToken){
            v = CalculateUtils.toLong(left.value)& CalculateUtils.toLong(right.value);
            context.getVariableOperation().setVariableValue(left.variable,v);
            return v;
        }
        else if(context.operatorSymbolConfig.XOR_ASSIGNMENT.token==operatorToken){
            v = CalculateUtils.toLong(left.value)^ CalculateUtils.toLong(right.value);
            context.getVariableOperation().setVariableValue(left.variable,v);
            return v;
        }
        else if(context.operatorSymbolConfig.REMAINDER_ASSIGNMENT.token==operatorToken){
            v = CalculateUtils.toLong(left.value)% CalculateUtils.toLong(right.value);
            context.getVariableOperation().setVariableValue(left.variable,v);
            return v;
        }
        else if(context.operatorSymbolConfig.DIVIDE_ASSIGNMENT.token==operatorToken){
            v = CalculateUtils.divide(left.value, right.value);
            context.getVariableOperation().setVariableValue(left.variable,v);
            return v;
        }
        else if(context.operatorSymbolConfig.MULTIPLY_ASSIGNMENT.token==operatorToken){
            v = CalculateUtils.multiply(left.value, right.value);
            context.getVariableOperation().setVariableValue(left.variable,v);
            return v;
        }
        else if(context.operatorSymbolConfig.MINUS_ASSIGNMENT.token==operatorToken){
            v = CalculateUtils.subtract(left.value, right.value);
            context.getVariableOperation().setVariableValue(left.variable,v);
            return v;
        }
        else if(context.operatorSymbolConfig.ADD_ASSIGNMENT.token==operatorToken){
            v = CalculateUtils.add(left.value, right.value);
            context.getVariableOperation().setVariableValue(left.variable,v);
            return v;
        }
        else if(context.operatorSymbolConfig.OR2.token==operatorToken){
            v = CalculateUtils.toBoolean(left.value)|| CalculateUtils.toBoolean(right.value);
            return v;
        }
        else if(context.operatorSymbolConfig.AND2.token==operatorToken){
            v = CalculateUtils.toBoolean(left.value)&& CalculateUtils.toBoolean(right.value);
            return v;
        }
        else if(context.operatorSymbolConfig.OR.token==operatorToken){
            return CalculateUtils.toInteger(left.value)| CalculateUtils.toInteger(right.value);
        }
        else if(context.operatorSymbolConfig.XOR.token==operatorToken){
            return CalculateUtils.toInteger(left.value)^ CalculateUtils.toInteger(right.value);
        }
        else if(context.operatorSymbolConfig.AND.token==operatorToken){
            return CalculateUtils.toInteger(left.value)& CalculateUtils.toInteger(right.value);
        }
        else if(context.operatorSymbolConfig.UNEQUAL.token==operatorToken){
            return CalculateUtils.unEquals(left.value,right.value);
        }
        else if(context.operatorSymbolConfig.EQUAL.token==operatorToken){
            return CalculateUtils.equals(left.value,right.value);
        }
        else if(context.operatorSymbolConfig.LESS_EQUAL.token==operatorToken){
            return CalculateUtils.lessOrEquals(left.value,right.value);
        }
        else if(context.operatorSymbolConfig.GREATER_EQUAL.token==operatorToken){
            return CalculateUtils.greaterOrEquals(left.value,right.value);
        }
        else if(context.operatorSymbolConfig.LESS.token==operatorToken){
            return CalculateUtils.less(left.value,right.value);
        }
        else if(context.operatorSymbolConfig.GREATER.token==operatorToken){
            return CalculateUtils.greater(left.value,right.value);
        }
        else if(context.operatorSymbolConfig.RIGHT_MOVE.token==operatorToken){
            return CalculateUtils.toInteger(left.value)>> CalculateUtils.toInteger(right.value);
        }
        else if(context.operatorSymbolConfig.LEFT_MOVE.token==operatorToken){
            return CalculateUtils.toInteger(left.value)<< CalculateUtils.toInteger(right.value);
        }
        else if(context.operatorSymbolConfig.RIGHT_MOVE2.token==operatorToken){
            return CalculateUtils.toLong(left.value)>>> CalculateUtils.toLong(right.value);
        }
        else if(context.operatorSymbolConfig.MINUS.token==operatorToken){
            return CalculateUtils.subtract(left.value,right.value);
        }
        else if(context.operatorSymbolConfig.ADDITIVE.token==operatorToken){
            return CalculateUtils.add(left.value,right.value);
        }
        else if(context.operatorSymbolConfig.REMAINDER.token==operatorToken){
            return CalculateUtils.toInteger(left.value)% CalculateUtils.toInteger(right.value);
        }
        else if(context.operatorSymbolConfig.DIVIDE.token==operatorToken){
            return CalculateUtils.divide(left.value,right.value);
        }
        else if(context.operatorSymbolConfig.MULTIPLY.token==operatorToken){
            return CalculateUtils.multiply(left.value,right.value);
        }
//        else if(OperatorConstant.DECREMENT.token==operatorToken){
//            return left.value;
//        }
//        else if(OperatorConstant.INCREMENTAL.token==operatorToken){
//            return left.value;
//        }else {
//            return left.value;
//        }
		LsRuntimeException.CodeErrorDetail detail=new LsRuntimeException.CodeErrorDetail();
		detail.message="未知运算符token '"+operatorToken+"' !";
		throw new LsRuntimeException(detail);
    }

    public int getOperatorLevel(LsExecuteContext context,int operatorToken){

        for (OperatorValue operatorValue : operatorValues) {
            if(operatorValue.canOperator(context,operatorToken)){
               return operatorValue.getOperatorLevel(context,operatorToken);
            }
        }
        return context.operatorSymbolConfig.getOperatorLevel(operatorToken);
    }

 
    /**
     * 将结果根据运算符优先级计算出来
     * @param results
     * @return
     */

    public Value statisticsResult(LsExecuteContext context,List<Value> results) {
        if(results.size()==1)
            return results.get(0);
        List<Integer> levels=new ArrayList<>();
        for (Value result : results) {
            result.operatorLevel =getOperatorLevel(context,result.operatorToken);
            if(!levels.contains(result.operatorLevel)){
                levels.add(result.operatorLevel);
            }
        }
        Object[] levelsArray = levels.toArray();
        Arrays.sort(levelsArray,Collections.reverseOrder());
        int size=results.size();
        for (Object level:levelsArray) {
            for (int index = 1; index<size; index++) {
                Value calculate = results.get(index);
                if(calculate.operatorLevel == CalculateUtils.toInteger(level)){
                    Value up=results.get(index-1);
                    up.value=calculate(context,up,calculate.operatorToken,calculate);
                    up.start = Math.max(calculate.start, up.start);
                    results.remove(index);
                    index--;
                    size--;
                }
            }
        }
        return results.get(0);
    }

    private  int skipNext(LsExecuteContext context, List<CodeNode> codeNodes, int start) {
        if (start + 1 >= codeNodes.size()) return start;
        CodeNode node = codeNodes.get(start + 1);
        int token=node.getFirstWordToken();
        for (OperatorValue operatorValue : operatorValues) {
            if (operatorValue.canOperator(context,token)) {
                return skipNext(context, codeNodes, operatorValue.skipOperatorValue(context, codeNodes, start + 1, token));
            }
        }
        if (context.operatorSymbolConfig.DOUBT.token==token) {
            start += 2;
            start = NodeUtils.findNodeByToken(codeNodes, context.keyWordConfig.PARTITION_KEYWORD.token, start) + 1;
            start = skipNext(context,codeNodes, context.skipValue( codeNodes, start));
        }  else {
            if(getOperatorLevel(context,token)!=context.operatorSymbolConfig.DEFAULT.operatorLevel) {
                start += 2;
                start = skipNext(context,  codeNodes, context.skipValue( codeNodes, start));
            }
        }
        return start;
    }

    public interface OperatorValue{
        boolean canOperator(LsExecuteContext context,int operatorToken);
        int getOperatorValue(LsExecuteContext contextr, ScriptFile scriptFile, Map<String, Object> variableMap, List<CodeNode> codeNodes, int start, List<Value> operatorValuesOut, int operatorToken, OperatorHandler operatorHandler);
         int getOperatorLevel(LsExecuteContext context,int operatorToken);
        int skipOperatorValue(LsExecuteContext contextr, List<CodeNode> codeNodes, int start, int operatorToken);
        Object operator(LsExecuteContext context,Value left, int operatorToken, Value right);

    }
}
