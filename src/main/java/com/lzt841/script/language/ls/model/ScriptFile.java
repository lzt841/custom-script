package com.lzt841.script.language.ls.model;

import com.lzt841.script.grammar.CodeNode;

import java.util.HashMap;
import java.util.Map;

public class ScriptFile {

    public CodeNode node;
    public Map<String, Object> variableMap=new HashMap<>();
    public ScriptFile(CodeNode node) {
       this.node=node;
    }

    public static class Function{

        public ScriptFile scriptFile;
       public CodeNode funNode;
       public CodeNode parameterNode;

       public Map<String,Object> variableMap;
    }

}