package com.lzt841.script.language.ls.function.instances.variable;

import com.lzt841.script.language.ls.function.VariableFunction;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class VariableKeysFunction implements VariableFunction {
    @Override
    public Object execute(Object variable, List parameters) {
        if (List.class.isInstance(variable)) {
            return listKeys((List)variable,parameters);
        } else if (Map.class.isInstance(variable)) {
            return mapKeys((Map)variable,parameters);
        }
        return new ArrayList<>();
    }

    private Object mapKeys(Map variable, List parameters) {
        List<Object> keys=new ArrayList<>();
        for (Object o : variable.keySet()) {
            keys.add(o);
        }
        return keys;
    }

    private Object listKeys(List variable, List parameters) {
        int size=variable.size();
        List<Object> keys=new ArrayList<>();
        for (int i = 0; i < size; i++) {
            keys.add(i);
        }
        return keys;
    }
}
