package com.lzt841.script.language.ls.execute;

import com.lzt841.script.grammar.CodeNode;
import com.lzt841.script.language.ls.LsExecuteContext;
import com.lzt841.script.language.ls.model.Result;
import com.lzt841.script.language.ls.model.ScriptFile;
import com.lzt841.script.language.ls.model.Variable;
import com.lzt841.script.language.ls.utils.CalculateUtils;
import com.lzt841.script.language.ls.utils.NodeUtils;

import java.util.List;
import java.util.Map;

public class LogicExecute implements ExecuteHandler
{

       @Override
    public boolean canExecute(LsExecuteContext context, List<CodeNode> codeNodes, int i) {
           CodeNode node= codeNodes.get(i);
           int token=node.getFirstWordToken();
           return token== context.keyWordConfig.IF_KEYWORD.token||token== context.keyWordConfig.WHILE_KEYWORD.token
                   ||token== context.keyWordConfig.DO_KEYWORD.token||token== context.keyWordConfig.FOR_KEYWORD.token;
    }
    @Override
    public int skipExecute(LsExecuteContext context, List<CodeNode> codeNodes, int i)
    {
        CodeNode node= codeNodes.get(i);
        int token=node.getFirstWordToken();

        if( context.keyWordConfig.IF_KEYWORD.token==token) {
            i=skipIf(context,codeNodes,i);
        }else if(context.keyWordConfig.WHILE_KEYWORD.token==token){
            i= context.skipResult(codeNodes,i+1);
            i= context.skipExecute(codeNodes,i+1);
        }else if(context.keyWordConfig.DO_KEYWORD.token==token){
            i= context.skipExecute(codeNodes,i+1);
            i= context.skipResult(codeNodes,i+2);
        }else if(context.keyWordConfig.FOR_KEYWORD.token==token){
            i= context.skipResult(codeNodes,i+1);
            i= context.skipExecute(codeNodes,i+1);
        }
        return i;
    }

    private int skipIf(LsExecuteContext executeHandle,  List<CodeNode> codeNodes, int i) {
        i = executeHandle.skipResult(codeNodes, i + 1);
        i=executeHandle.skipExecute(codeNodes,i+1);
        int size = codeNodes.size();
        if(i+1<size){
            int token=codeNodes.get(i+1).getFirstWordToken();
            i++;
            if(token== executeHandle.keyWordConfig.ELSE_KEYWORD.token){
                i=executeHandle.skipExecute(codeNodes,i+1);
            }
        }
        return i;
    }


    @Override
    public int execute(LsExecuteContext context, ScriptFile scriptFile, Map<String, Object> variableMap, List<CodeNode> codeNodes, int start, Result funResult) {
        CodeNode node= codeNodes.get(start);
        int token=node.getFirstWordToken();
        Result result;
        if( context.keyWordConfig.IF_KEYWORD.token==token) {
                start = logicIf(false, context, scriptFile, variableMap, codeNodes, start, funResult);
        } else if( context.keyWordConfig.WHILE_KEYWORD.token==token) {
                result = new Result();
                start += 1;//while()
                int logic = start;//逻辑执行位置
                while (true) {
                    result.resultCode = Result.NORMAL;
                    result.result = null;
                    start = context.getResult(scriptFile, codeNodes, variableMap, logic, result);//while()
                    start++;//while(){}
                    if (!CalculateUtils.isTrue(result.result)) {
                        start = context.skipExecute( codeNodes, start);
                        break;
                    }
                    if (start < codeNodes.size()) {
                        int nextToken = codeNodes.get(start).getFirstWordToken();
                        if (context.keyWordConfig.EXECUTOR_START_KEYWORD.token == nextToken) {
                            List<CodeNode> childrens = codeNodes.get(start).childrens;
                            Result result1 = new Result();
                            context.run(scriptFile, childrens, variableMap, 1, childrens.size(), result1);
                            if (result1.isUnNormal()) {
                                funResult.copy(result1);
                                if (result1.isReturn()) {
                                    start = codeNodes.size() - 1;
                                    break;
                                } else if (result1.isBreak()) {
                                    start = context.skipExecute( codeNodes, start);
                                    break;
                                }
                            }
                        } else {
                            Result result1 = new Result();
                            context.run(scriptFile, codeNodes, variableMap, start, start + 1, result1);
                            if (result1.isUnNormal()) {
                                funResult.copy(result1);
                                if (result1.isReturn()) {
                                    start = codeNodes.size() - 1;
                                    break;
                                } else if (result1.isBreak()) {
                                    start = context.skipExecute( codeNodes, start);
                                    break;
                                }
                            }
                        }
                    }

                }
            }
            else if( context.keyWordConfig.DO_KEYWORD.token==token) {
                result = new Result();
                start += 1;//do{}
                int old = start;//do执行位置
                do {
                    start = old;
                    if (start < codeNodes.size()) {
                        int nextToken = codeNodes.get(start).getFirstWordToken();
                        if (context.keyWordConfig.EXECUTOR_START_KEYWORD.token == nextToken) {
                            List<CodeNode> childrens = codeNodes.get(start).childrens;
                            Result result1 = new Result();
                            context.run(scriptFile, childrens, variableMap, 1, childrens.size(), result1);
                            if (result1.isUnNormal()) {
                                funResult.copy(result1);
                                if (result1.isReturn()) {
                                    start = codeNodes.size() - 1;
                                    break;
                                } else if (result1.isBreak()) {
                                    start = context.skipExecute( codeNodes, old);
                                    start = context.skipExecute( codeNodes, start + 2);
                                    break;
                                }
                            }
                        } else {
                            Result result1 = new Result();
                            start = context.run(scriptFile, codeNodes, variableMap, start, start + 1, result1) - 1;
                            if (result1.isUnNormal()) {
                                funResult.copy(result1);
                                if (result1.isReturn()) {
                                    start = codeNodes.size() - 1;
                                    break;
                                } else if (result1.isBreak()) {
                                    start = context.skipExecute( codeNodes, old);
                                    start = context.skipExecute( codeNodes, start + 2);
                                    break;
                                }
                            }
                        }
                    }
                    result.resultCode = Result.NORMAL;
                    result.result = null;
                    start = context.getResult(scriptFile, codeNodes, variableMap, start + 2, result);//do{}while()

                } while (CalculateUtils.isTrue(result.result));
            }
            else if( context.keyWordConfig.FOR_KEYWORD.token==token) {
                result = new Result();
                start += 1;//for()
                int logic = start;//逻辑执行位置
                List<CodeNode> childrens1 = codeNodes.get(start).childrens;
                int one = NodeUtils.findNodeByToken(childrens1, context.keyWordConfig.SEPARATE_KEYWORD.token, 1);
                if (one > 0) {//例子:for(a=1;a<10;a++){}  for(;i<10;i++){}
                    int tow = NodeUtils.findNodeByToken(childrens1, context.keyWordConfig.SEPARATE_KEYWORD.token, one + 1);
                    //init
                    context.run(scriptFile, childrens1, variableMap, 1, one, new Result());

                    while (true) {
                        result.resultCode = Result.NORMAL;
                        result.result = null;
                        context.getResult(scriptFile, childrens1, variableMap, one + 1, result);//for()
                        start = logic + 1;//for(){}
                        if (!CalculateUtils.isTrue(result.result)) {
                            start = context.skipExecute( codeNodes, start);
                            break;
                        }
                        if (start < codeNodes.size()) {
                            int nextToken = codeNodes.get(start).getFirstWordToken();
                            if (context.keyWordConfig.EXECUTOR_START_KEYWORD.token == nextToken) {
                                List<CodeNode> childrens = codeNodes.get(start).childrens;
                                Result result1 = new Result();
                                context.run(scriptFile, childrens, variableMap, 1, childrens.size(), result1);
                                if (result1.isUnNormal()) {
                                    funResult.copy(result1);
                                    if (result1.isReturn()) {
                                        start = codeNodes.size() - 1;
                                        break;
                                    } else if (result1.isBreak()) {
                                        start = context.skipExecute( codeNodes, start);
                                        break;
                                    }
                                }
                            } else {
                                Result result1 = new Result();
                                context.run(scriptFile, codeNodes, variableMap, start, start + 1, result1);
                                if (result1.isUnNormal()) {
                                    funResult.copy(result1);
                                    if (result1.isReturn()) {
                                        start = codeNodes.size() - 1;
                                        break;
                                    } else if (result1.isBreak()) {
                                        start = context.skipExecute( codeNodes, start);
                                        break;
                                    }
                                }
                            }
                        }
                        context.run(scriptFile, childrens1, variableMap, tow + 1, childrens1.size(), new Result());
                    }
                } else if ((one = NodeUtils.findNodeByToken(childrens1, context.keyWordConfig.PARTITION_KEYWORD.token, 1)) > 0) {
                    //例子:for(a:list){}
                    Variable var = CalculateUtils.findVariableLeft(context, scriptFile, variableMap, childrens1, one - 1);
                    Variable list = CalculateUtils.findVariableRight(context, scriptFile, variableMap, childrens1, one + 1);
                    Object oldVar = null;
                    var.library = variableMap;
                    boolean existence;
                    if ((existence = context.getVariableOperation().isExistence(var))) {
                        oldVar = context.getVariableOperation().getVariableValue(var);
                    }
                    start = logic + 1;
                    Object value =context.getVariableOperation().getVariableValue(list);
                    int nextToken = codeNodes.get(start).getFirstWordToken();
                    List<CodeNode> childrens = codeNodes.get(start).childrens;

                    Result result1 = new Result();
                    if (value instanceof Map) {
                        Object[] objects = ((Map<?, ?>) value).keySet().toArray();
                        int size = objects.length;
                        int i = 0;
                        while (true) {
                            if (i >= size) {
                                start = context.skipExecute( codeNodes, logic + 1);
                                break;
                            }
                            context.getVariableOperation().setVariableValue(var,objects[i]);
                            result1.resultCode = Result.NORMAL;
                            result1.result = null;
                            if (context.keyWordConfig.EXECUTOR_START_KEYWORD.token == nextToken) {
                                context.run(scriptFile, childrens, variableMap, 1, childrens.size(), result1);
                            } else {
                                context.run(scriptFile, codeNodes, variableMap, start, start + 1, result1);
                            }
                            if (result1.isUnNormal()) {
                                funResult.copy(result1);
                                if (result1.isReturn()) {
                                    start = codeNodes.size() - 1;
                                    break;
                                } else if (result1.isBreak()) {
                                    start = context.skipExecute(codeNodes, logic + 1);
                                    break;
                                }
                            }
                            i++;
                        }

                    } else if (value instanceof List) {
                        int size = ((List) value).size();
                        int i = 0;
                        while (true) {
                            if (i >= size) {
                                start = context.skipExecute( codeNodes, logic + 1);
                                break;
                            }
                            context.getVariableOperation().setVariableValue(var,i);
                            result1.resultCode = Result.NORMAL;
                            result1.result = null;
                            if (context.keyWordConfig.EXECUTOR_START_KEYWORD.token == nextToken) {
                                context.run(scriptFile, childrens, variableMap, 1, childrens.size(), result1);
                            } else {
                                context.run(scriptFile, codeNodes, variableMap, start, start + 1, result1);
                            }
                            if (result1.isUnNormal()) {
                                funResult.copy(result1);
                                if (result1.isReturn()) {
                                    start = codeNodes.size() - 1;
                                    break;
                                } else if (result1.isBreak()) {
                                    start = context.skipExecute( codeNodes, logic + 1);
                                    break;
                                }
                            }
                            i++;
                        }
                    }
                    if (existence) {
                        context.getVariableOperation().setVariableValue(var,oldVar);
                    }
                }
            }

        return start;
    }

    private int logicIf(boolean upIf, LsExecuteContext executeHandle, ScriptFile scriptFile, Map<String, Object> variableMap, List<CodeNode> codeNodes, int start, Result funResult) {
       if(!upIf) {
           Result result = new Result();
           start += 1;//if()
           start = executeHandle.getResult(scriptFile, codeNodes, variableMap, start, result);//if()
           start++;//if(){}
           boolean ok = CalculateUtils.isTrue(result.result);
           if (ok) {
                   Result result1=new Result();
                   start=executeHandle.run(scriptFile,codeNodes,variableMap,start,start+1,result1)-1;
                   if (result1.isUnNormal()) {
                       funResult.copy(result1);
                       start = codeNodes.size() - 1;
                   }
           }else{
               start=executeHandle.skipExecute(codeNodes,start);
           }
           if (start + 2 < codeNodes.size()) {//if(){}else{}
               int nextToken=codeNodes.get(start+1).getFirstWordToken();//if(){}else
               if (executeHandle.keyWordConfig.ELSE_KEYWORD.token==nextToken) {
                   start += 2;//if(){}else{} || if(){}else if
                   CodeNode elseRunNode = codeNodes.get(start);
                   int runToken = elseRunNode.getFirstWordToken();
                  if (executeHandle.keyWordConfig.IF_KEYWORD.token==runToken) {//if(){}else if
                       return logicIf(ok,executeHandle,scriptFile,variableMap,codeNodes,start,funResult);
                   } else {
                       if(!ok) {
                           Result result1 = new Result();
                           start=executeHandle.run(scriptFile, codeNodes, variableMap, start, start + 1, result1)-1;
                           if (result1.isUnNormal()) {
                               funResult.copy(result1);
                               start = codeNodes.size() - 1;
                           }
                       }else {
                           start=executeHandle.skipExecute(codeNodes,start);
                       }
                   }
               }
           }
       }else {
           //不用执行if内语句,而是跳过指针
           start=executeHandle.skipResult(codeNodes,start+1);//if()
           start=executeHandle.skipExecute(codeNodes,start+1);//if(){}
           if (start + 2 < codeNodes.size()) {//if(){}else X
               int nextToken=codeNodes.get(start+1).getFirstWordToken();
                if(executeHandle.keyWordConfig.ELSE_KEYWORD.token==nextToken){
                    start+=2;
                    int nextToken1=codeNodes.get(start).getFirstWordToken();
                    if(executeHandle.keyWordConfig.IF_KEYWORD.token==nextToken1){//else if
                        return logicIf(true,executeHandle,scriptFile,variableMap,codeNodes,start,funResult);
                    }else{//else xxxx
                       start=executeHandle.skipExecute(codeNodes,start);
                    }
                }
           }
           return start;
       }
       return start;

    }
}
