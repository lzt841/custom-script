package com.lzt841.script.language.ls.exception;

public class VariableNoFoundException extends LsRuntimeException {
    public static final int ERROR_CODE=1;
    public VariableNoFoundException(CodeErrorDetail detail) {
        super(detail);
        detail.errorCode=ERROR_CODE;
    }
}
