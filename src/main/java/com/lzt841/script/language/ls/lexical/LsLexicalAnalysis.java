package com.lzt841.script.language.ls.lexical;

import com.lzt841.script.language.ls.LsExecuteContext;
import com.lzt841.script.language.ls.model.Keyword;
import com.lzt841.script.language.ls.utils.KeyWordConfig;
import com.lzt841.script.language.ls.utils.OperatorSymbolConfig;
import com.lzt841.script.lexical.CharHandler;
import com.lzt841.script.lexical.LexicalAnalysis;
import com.lzt841.script.lexical.Word;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class LsLexicalAnalysis extends LexicalAnalysis {
    private final LsExecuteContext context;

    public LsLexicalAnalysis(LsExecuteContext context) {
        super(false);
        this.context = context;
        handlers=init();

        addKeyWord(context.keyWordConfig.FUNCTION_KEYWORD);
        addKeyWord(context.keyWordConfig.GLOBAL_KEYWORD);
        addKeyWord(context.keyWordConfig.SELF_KEYWORD);
        addKeyWord(context.keyWordConfig.IF_KEYWORD);
        addKeyWord(context.keyWordConfig.ELSE_KEYWORD);
        addKeyWord(context.keyWordConfig.WHILE_KEYWORD);
        addKeyWord(context.keyWordConfig.FOR_KEYWORD);
        addKeyWord(context.keyWordConfig.DO_KEYWORD);
        addKeyWord(context.keyWordConfig.ARRAY_LENGTH_KEYWORD);
        addKeyWord(context.keyWordConfig.FALSE_KEYWORD);
        addKeyWord(context.keyWordConfig.TRUE_KEYWORD);
        addKeyWord(context.keyWordConfig.NULL_KEYWORD);
        addKeyWord(context.keyWordConfig.RETURN_KEYWORD);
        addKeyWord(context.keyWordConfig.BREAK_KEYWORD);
        addKeyWord(context.keyWordConfig.CONTINUE_KEYWORD);
    }

    public void addKeyWord(Keyword keyword) {
        keywords.put(keyword.keyword, keyword.token);
    }

    private List<CharHandler> init() {

        CharHandler[] handler=new CharHandler[]{

			new LsRegionCharHandler(context.keyWordConfig.ANNOTATION_KEYWORD, context.keyWordConfig.ENTER_KEYWORD,true),
                new LsRegionCharHandler(context.keyWordConfig.ANNOTATION_START_KEYWORD, context.keyWordConfig.ANNOTATION_END_KEYWORD,true),
                new LsStringCharHandler(context.keyWordConfig.STRING1_START_KEYWORD, context.keyWordConfig.STRING1_END_KEYWORD),
                new LsStringCharHandler(context.keyWordConfig.STRING2_START_KEYWORD, context.keyWordConfig.STRING2_END_KEYWORD),


			new LsSymbolCharHandler(context.operatorSymbolConfig.NOT),
                new LsSymbolCharHandler(context.operatorSymbolConfig.ASSIGNMENT),
                new LsSymbolCharHandler(context.operatorSymbolConfig.DOUBT),
                new LsSymbolCharHandler(context.operatorSymbolConfig.RIGHT_MOVE2_ASSIGNMENT),
                new LsSymbolCharHandler(context.operatorSymbolConfig.RIGHT_MOVE_ASSIGNMENT),
                new LsSymbolCharHandler(context.operatorSymbolConfig.LEFT_MOVE_ASSIGNMENT),
                new LsSymbolCharHandler(context.operatorSymbolConfig.OR_ASSIGNMENT),
                new LsSymbolCharHandler(context.operatorSymbolConfig.AND_ASSIGNMENT),
                new LsSymbolCharHandler(context.operatorSymbolConfig.XOR_ASSIGNMENT),
                new LsSymbolCharHandler(context.operatorSymbolConfig.REMAINDER_ASSIGNMENT),
                new LsSymbolCharHandler(context.operatorSymbolConfig.DIVIDE_ASSIGNMENT),
                new LsSymbolCharHandler(context.operatorSymbolConfig.MULTIPLY_ASSIGNMENT),
                new LsSymbolCharHandler(context.operatorSymbolConfig.MINUS_ASSIGNMENT),
                new LsSymbolCharHandler(context.operatorSymbolConfig.ADD_ASSIGNMENT),
                new LsSymbolCharHandler(context.operatorSymbolConfig.OR2),
                new LsSymbolCharHandler(context.operatorSymbolConfig.AND2),
                new LsSymbolCharHandler(context.operatorSymbolConfig.OR),
                new LsSymbolCharHandler(context.operatorSymbolConfig.XOR),
                new LsSymbolCharHandler(context.operatorSymbolConfig.AND),
                new LsSymbolCharHandler(context.operatorSymbolConfig.UNEQUAL),
                new LsSymbolCharHandler(context.operatorSymbolConfig.EQUAL),
                new LsSymbolCharHandler(context.operatorSymbolConfig.LESS_EQUAL),
                new LsSymbolCharHandler(context.operatorSymbolConfig.GREATER_EQUAL),
                new LsSymbolCharHandler(context.operatorSymbolConfig.LESS),
                new LsSymbolCharHandler(context.operatorSymbolConfig.GREATER),
                new LsSymbolCharHandler(context.operatorSymbolConfig.RIGHT_MOVE),
                new LsSymbolCharHandler(context.operatorSymbolConfig.LEFT_MOVE),
                new LsSymbolCharHandler(context.operatorSymbolConfig.RIGHT_MOVE2),
                new LsSymbolCharHandler(context.operatorSymbolConfig.MINUS),
                new LsSymbolCharHandler(context.operatorSymbolConfig.ADDITIVE),
                new LsSymbolCharHandler(context.operatorSymbolConfig.REMAINDER),
                new LsSymbolCharHandler(context.operatorSymbolConfig.DIVIDE),
                new LsSymbolCharHandler(context.operatorSymbolConfig.MULTIPLY),
                new LsSymbolCharHandler(context.operatorSymbolConfig.DECREMENT),
                new LsSymbolCharHandler(context.operatorSymbolConfig.INCREMENTAL),



                new LsSymbolCharHandler(context.keyWordConfig.PARTITION_KEYWORD),
                new LsSymbolCharHandler(context.keyWordConfig.INTERVAL_KEYWORD),
                new LsSymbolCharHandler(context.keyWordConfig.SEPARATE_KEYWORD),
                new LsSymbolCharHandler(context.keyWordConfig.PARAMETER_START_KEYWORD),
                new LsSymbolCharHandler(context.keyWordConfig.PARAMETER_END_KEYWORD),
                new LsSymbolCharHandler(context.keyWordConfig.EXECUTOR_START_KEYWORD),
                new LsSymbolCharHandler(context.keyWordConfig.EXECUTOR_END_KEYWORD),
                new LsSymbolCharHandler(context.keyWordConfig.OBJECT_START_KEYWORD),
                new LsSymbolCharHandler(context.keyWordConfig.OBJECT_END_KEYWORD),
                new LsSymbolCharHandler(context.keyWordConfig.ARRAY_START_KEYWORD),
                new LsSymbolCharHandler(context.keyWordConfig.ARRAY_END_KEYWORD),
                new LsSymbolCharHandler(context.keyWordConfig.SUBSET_LINK_KEYWORD),
                new LsSymbolCharHandler(context.keyWordConfig.FUNCTION_ARROW_KEYWORD),
                new LsSymbolCharHandler(context.keyWordConfig.SUBSET_START_KEYWORD),
                new LsSymbolCharHandler(context.keyWordConfig.SUBSET_END_KEYWORD),

                new LsSymbolCharHandler(" ", true,0),
                new LsSymbolCharHandler("\f", true,0),
                new LsSymbolCharHandler("\n", true,0),
                new LsSymbolCharHandler("\t", true,0),
                new LsSymbolCharHandler("\r", true,0),
                new LsDefaultCharHandler(context.keyWordConfig.DEFAULT_KEYWORD)
        };
        List<CharHandler> ha= Arrays.asList(handler);
        Collections.sort(ha, new Comparator<CharHandler>(){
            @Override
            public int compare(CharHandler o1, CharHandler o2) {
                return o2.getSort()-o1.getSort();
            }
        });
        return ha;
    }
	
    @Override
    public List<Word> analysis(CharSequence codeText) {
        return analysis(codeText,new ArrayList<Word>());
    }
    @Override
    public List<Word> analysis(CharSequence codeText, List<Word> out) {
        List<Word> analysis = super.analysis(codeText, out);
        handlerNumber(analysis);
        handlerString(analysis);
        return analysis;
    }

    private void handlerNumber(List<Word> analysis) {

        for (int i = 0; i < analysis.size(); i++) {
            Word word = analysis.get(i);
            if(".".equals(word.word)){
                String v1=null,v2=null;
                if(i>0)v1=analysis.get(i-1).word;
                if(i+1<analysis.size())v2=analysis.get(i+1).word;
                try{
                    double v = Double.parseDouble(v1 + "." + v2);
                    analysis.get(i-1).word+="."+v2;
                    analysis.get(i-1).end=analysis.get(i+1).end;
                    char c = v2.charAt(v2.length() - 1);
                    analysis.get(i-1).data=(c=='f'||c=='F')?Float.parseFloat(v1+"."+v2):v;
                    analysis.get(i-1).token=context.keyWordConfig.NUMBER_KEYWORD.token;
                    analysis.remove(i);
                    analysis.remove(i);
                    i--;
                }catch (Exception e){

                }
            }else {
                try {
                    long l = Long.parseLong(word.word);
                    char c = word.word.charAt(word.word.length() - 1);
                    word.data=(c=='l'||c=='L')?l:Integer.parseInt(word.word);
                    word.token=context.keyWordConfig.NUMBER_KEYWORD.token;
                }catch (Exception e){

                }
            }
        }
    }

    /**
     * 处理字符串
     * @param analysis
     */
    private void handlerString(List<Word> analysis) {
        for (Word word : analysis) {
            if(word.token== context.keyWordConfig.STRING1_START_KEYWORD.token) {
                word.word = word.word.substring(context.keyWordConfig.STRING1_START_KEYWORD.keyword.length(), word.word.length() - context.keyWordConfig.STRING1_END_KEYWORD.keyword.length());
                word.word = stringParaphrase(word.word);
            }
            else if(word.token== context.keyWordConfig.STRING2_START_KEYWORD.token) {
                word.word= word.word.substring(context.keyWordConfig.STRING2_START_KEYWORD.keyword.length(),word.word.length()- context.keyWordConfig.STRING2_END_KEYWORD.keyword.length());
                word.word = stringParaphrase(word.word);
            }
        }
    }
    public static String stringParaphrase(String string){
        string=string.replaceAll("\\\\n","\n");
        string=string.replaceAll("\\\\t","\t");
        string=string.replaceAll("\\\\r","\r");
        string=string.replaceAll("\\\\'", "'");
        string=string.replaceAll("\\\\\"","\"");
        string=string.replaceAll("\\\\\\\\","\\");
        return string;
    }


}
