package com.lzt841.script.language.ls.model;

public  class Result{
    public static final int NORMAL=0, RETURN=1,BREAK=2,CONTINUE=3;
    public Object result;
    public int resultCode =NORMAL;

    public Result(){

    }
    public Result(Object result) {
        this.result = result;
    }

    public Result copy(Result src){
        resultCode=src.resultCode;
        result=src.result;
        return this;
    }
    public boolean isReturn(){
        return resultCode==RETURN;
    }
    public boolean isBreak(){
        return resultCode==BREAK;
    }
    public boolean isCONTINUE(){
        return resultCode==CONTINUE;
    }
    public boolean isNormal(){
        return resultCode==NORMAL;
    }
    public boolean isUnNormal(){
        return resultCode!=NORMAL;
    }
}
