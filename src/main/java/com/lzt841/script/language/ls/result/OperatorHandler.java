package com.lzt841.script.language.ls.result;

import com.lzt841.script.language.ls.LsExecuteContext;
import com.lzt841.script.language.ls.model.Value;

import java.util.List;

public interface OperatorHandler {
    Value statisticsResult(LsExecuteContext context, List<Value> values);
}
