package com.lzt841.script.language.ls.function.instances.variable;

import com.lzt841.script.language.ls.function.VariableFunction;
import com.lzt841.script.language.ls.utils.CalculateUtils;

import java.util.List;
import java.util.Map;

public class VariableAddFunction implements VariableFunction {
    @Override
    public Object execute(Object variable, List parameters) {

        if(parameters.size()==0)return null;
        if (Map.class.isInstance(variable)) {
            return addMap((Map) variable,parameters);
        }else if (List.class.isInstance(variable)) {
            return addList((List) variable,parameters);
        }else if(CharSequence.class.isInstance(variable)){
            for (Object parameter : parameters) {
                variable+=parameter+"";
            }
            return variable;
        }
        return null;
    }

    private Object addMap(Map variable, List parameters) {
        if(parameters.size()<2)return null;
        return variable.put(parameters.get(0),parameters.get(1));
    }

    private Object addList(List variable, List parameters) {
        if(parameters.size()==1){
            variable.add(parameters.get(0));

            return parameters.get(0);
        }
        variable.add(CalculateUtils.toInteger(parameters.get(0)),parameters.get(1));
        return parameters.get(1);
    }
}
