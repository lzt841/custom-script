package com.lzt841.script.language.ls.function.instances.variable;

import com.lzt841.script.language.ls.function.VariableFunction;
import com.lzt841.script.language.ls.utils.CalculateUtils;

import java.util.List;

public class VariableSubStringFunction implements VariableFunction {
    @Override
    public Object execute(Object variable, List parameters) {
        if(!CharSequence.class.isInstance(variable)||parameters.size()==0)return null;
        if(parameters.size()==1){
            return variable.toString().substring(CalculateUtils.toInteger(parameters.get(0)));
        }
        return variable.toString().substring(CalculateUtils.toInteger(parameters.get(0)), CalculateUtils.toInteger(parameters.get(1)));
    }
}
