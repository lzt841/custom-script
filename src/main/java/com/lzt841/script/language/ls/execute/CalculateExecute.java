package com.lzt841.script.language.ls.execute;


import com.lzt841.script.grammar.CodeNode;
import com.lzt841.script.language.ls.LsExecuteContext;
import com.lzt841.script.language.ls.model.Result;
import com.lzt841.script.language.ls.model.ScriptFile;

import java.util.List;
import java.util.Map;

public class CalculateExecute implements ExecuteHandler
{

	@Override
	public int skipExecute(LsExecuteContext context, List<CodeNode> codeNodes, int i)
	{
		CodeNode node = codeNodes.get(i);
        int wordToken = node.getFirstWordToken();
        if (context.keyWordConfig.ANNOTATION_KEYWORD.token == wordToken ||
			context.keyWordConfig.ANNOTATION_START_KEYWORD.token == wordToken ||
			context.keyWordConfig.ANNOTATION_END_KEYWORD.token == wordToken ||
			context.keyWordConfig.SEPARATE_KEYWORD.token == wordToken ||
			context.keyWordConfig.EXECUTOR_END_KEYWORD.token == wordToken ||
			context.keyWordConfig.SUBSET_LINK_KEYWORD.token == wordToken ||
			context.keyWordConfig.INTERVAL_KEYWORD.token == wordToken ||
			context.keyWordConfig.PARTITION_KEYWORD.token == wordToken||
            context.keyWordConfig.EXECUTOR_START_KEYWORD.token==wordToken) {
			return i;
        }else if( context.keyWordConfig.RETURN_KEYWORD.token==wordToken){
			i= context.skipResult(codeNodes,i+1);
		}
		else if( context.keyWordConfig.BREAK_KEYWORD.token==wordToken){
            i= context.skipResult(codeNodes,i+1);
		}else if( context.keyWordConfig.CONTINUE_KEYWORD.token==wordToken){
            i= context.skipResult(codeNodes,i+1);
		}else {
            i= context.skipResult(codeNodes,i);
        }
        return i;

	}



    @Override
    public boolean canExecute(LsExecuteContext context, List<CodeNode> codeNodes, int i) {
        return true;
    }
    @Override
    public int execute(LsExecuteContext context, ScriptFile scriptFile, Map<String, Object> variableMap, List<CodeNode> codeNodes, int i, Result funResult) {

        CodeNode node = codeNodes.get(i);
        int wordToken = node.getFirstWordToken();
        if (context.keyWordConfig.ANNOTATION_KEYWORD.token == wordToken ||
                context.keyWordConfig.ANNOTATION_START_KEYWORD.token == wordToken ||
                context.keyWordConfig.ANNOTATION_END_KEYWORD.token == wordToken ||
                context.keyWordConfig.SEPARATE_KEYWORD.token == wordToken ||
                context.keyWordConfig.EXECUTOR_END_KEYWORD.token == wordToken ||
                context.keyWordConfig.SUBSET_LINK_KEYWORD.token == wordToken ||
                context.keyWordConfig.INTERVAL_KEYWORD.token == wordToken ||
                context.keyWordConfig.PARTITION_KEYWORD.token == wordToken) {

        }else if( context.keyWordConfig.RETURN_KEYWORD.token==wordToken){
                Result result=new Result();
                context.getResult(scriptFile,codeNodes,variableMap,i+1,result);
                funResult.resultCode= Result.RETURN;
                funResult.result=result.result;
                i=codeNodes.size()-1;
        }else if( context.keyWordConfig.BREAK_KEYWORD.token==wordToken){
                funResult.resultCode= Result.BREAK;
                i=codeNodes.size()-1;
        }else if( context.keyWordConfig.CONTINUE_KEYWORD.token==wordToken){
                funResult.resultCode= Result.CONTINUE;
                i=codeNodes.size()-1;
        }else if( context.keyWordConfig.EXECUTOR_START_KEYWORD.token==wordToken){
                context.run(scriptFile,node.childrens,variableMap,1,node.childrens.size(),funResult);
        }else{
                Result result=new Result();
                i= context.getResult(scriptFile,codeNodes,variableMap,i,result);
        }
        return i;
    }


}
