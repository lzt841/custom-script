package com.lzt841.script.language.ls.function.instances.variable;

import com.lzt841.script.language.ls.function.VariableFunction;

import java.util.List;
import java.util.Map;

public class VariableClearFunction implements VariableFunction {
    @Override
    public Object execute(Object variable, List parameters) {
        if (Map.class.isInstance(variable)) {
             ((Map) variable).clear();
        }else if (List.class.isInstance(variable)) {
            ((List) variable).clear();
        }
        return variable;
    }
}
