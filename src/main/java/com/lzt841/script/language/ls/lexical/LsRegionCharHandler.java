package com.lzt841.script.language.ls.lexical;

import com.lzt841.script.language.ls.model.Keyword;
import com.lzt841.script.lexical.RegionCharHandler;

public class LsRegionCharHandler extends RegionCharHandler {
    public LsRegionCharHandler(Keyword keyword) {
        this(keyword,keyword,keyword.token);
    }
    public LsRegionCharHandler(Keyword startKeyword, Keyword endKeyword) {
        this(startKeyword, endKeyword,false, startKeyword.token);
    }
	public LsRegionCharHandler(Keyword startKeyword, Keyword endKeyword, boolean lose) {
        this(startKeyword, endKeyword,lose,startKeyword. token);
    }
    public LsRegionCharHandler(Keyword startKeyword, Keyword endKeyword, int token) {
        this(startKeyword, endKeyword,false, token);
    }
    public LsRegionCharHandler(Keyword startKeyword, Keyword endKeyword, boolean lose, int token) {
        super(startKeyword.keyword, endKeyword.keyword,lose, token);
    }

}
