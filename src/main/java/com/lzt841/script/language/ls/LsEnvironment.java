package com.lzt841.script.language.ls;

import com.lzt841.script.environment.EnvironmentHandler;
import com.lzt841.script.grammar.CodeNode;
import com.lzt841.script.language.ls.function.FunctionHandler;
import com.lzt841.script.language.ls.function.VariableFunction;
import com.lzt841.script.language.ls.function.instances.global.DebugFunction;
import com.lzt841.script.language.ls.function.instances.global.InputFunction;
import com.lzt841.script.language.ls.function.instances.global.PrintfFunction;
import com.lzt841.script.language.ls.function.instances.global.ReadLineFunction;
import com.lzt841.script.language.ls.function.instances.global.SleepFunction;
import com.lzt841.script.language.ls.function.instances.global.TimeFunction;
import com.lzt841.script.language.ls.function.instances.variable.VariableAddFunction;
import com.lzt841.script.language.ls.function.instances.variable.VariableClearFunction;
import com.lzt841.script.language.ls.function.instances.variable.VariableConcatFunction;
import com.lzt841.script.language.ls.function.instances.variable.VariableCopyFunction;
import com.lzt841.script.language.ls.function.instances.variable.VariableGetFunction;
import com.lzt841.script.language.ls.function.instances.variable.VariableHasFunction;
import com.lzt841.script.language.ls.function.instances.variable.VariableKeysFunction;
import com.lzt841.script.language.ls.function.instances.variable.VariableLengthFunction;
import com.lzt841.script.language.ls.function.instances.variable.VariablePushFunction;
import com.lzt841.script.language.ls.function.instances.variable.VariableRemoveFunction;
import com.lzt841.script.language.ls.function.instances.variable.VariableSetFunction;
import com.lzt841.script.language.ls.function.instances.variable.VariableSplitStringFunction;
import com.lzt841.script.language.ls.function.instances.variable.VariableSubStringFunction;
import com.lzt841.script.language.ls.grammar.LsGrammarTreeAnalysis;
import com.lzt841.script.language.ls.lexical.LsLexicalAnalysis;
import com.lzt841.script.language.ls.model.ScriptFile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.lzt841.script.language.ls.model.*;
import com.lzt841.script.language.ls.utils.KeyWordConfig;
import com.lzt841.script.language.ls.utils.OperatorSymbolConfig;

public class LsEnvironment implements EnvironmentHandler {

    private LsGrammarTreeAnalysis grammarTreeHandler;
    private LsLexicalAnalysis lexicalHandler;
    private LsExecuteContext executeContext;
    private Map<String, ScriptFile> context=new HashMap<>();


    protected Map<String, Object> globalVariableMap=new HashMap<>();

    private Map<String, VariableFunction> variableFunctionMap=new HashMap<>();

    private Map<String, FunctionHandler> globalFunctionMap=new HashMap<>();

    public LsEnvironment() {
        executeContext =new LsExecuteContext(this);
        grammarTreeHandler=new LsGrammarTreeAnalysis(executeContext);
        lexicalHandler=new LsLexicalAnalysis(executeContext);
        registerBaseFunction();
    }
    public LsEnvironment(KeyWordConfig keyWordConfig, OperatorSymbolConfig operatorSymbolConfig){
        executeContext=new LsExecuteContext(this,keyWordConfig,operatorSymbolConfig);
        grammarTreeHandler=new LsGrammarTreeAnalysis(executeContext);
        lexicalHandler=new LsLexicalAnalysis(executeContext);
        registerBaseFunction();
    }



    public LsEnvironment(LsGrammarTreeAnalysis grammarTreeHandler, LsLexicalAnalysis lexicalHandler) {
        this.grammarTreeHandler = grammarTreeHandler;
        this.lexicalHandler = lexicalHandler;
        this.executeContext = new LsExecuteContext(this);
        registerBaseFunction();
    }



	public void setGrammarTreeHandler(LsGrammarTreeAnalysis grammarTreeHandler)
	{
		this.grammarTreeHandler = grammarTreeHandler;
	}


	public LsGrammarTreeAnalysis getGrammarTreeHandler()
	{
		return grammarTreeHandler;
	}

	public void setLexicalHandler(LsLexicalAnalysis lexicalHandler)
	{
		this.lexicalHandler = lexicalHandler;
	}

	public LsLexicalAnalysis getLexicalHandler()
	{
		return lexicalHandler;
	}

	public void setExecuteContext(LsExecuteContext executeContext){
		this.executeContext = executeContext;
	}

	public LsExecuteContext getExecuteContext(){
		return executeContext;
	}
	
	
    private void registerBaseFunction(){
        registerBaseGlobalFunction();
        registerBaseVariableFunction();
    }

    private void registerBaseVariableFunction() {
        registerVariableFunction("length",new VariableLengthFunction());
        registerVariableFunction("has",new VariableHasFunction());
        registerVariableFunction("keys",new VariableKeysFunction());
        registerVariableFunction("add",new VariableAddFunction());
        registerVariableFunction("clear",new VariableClearFunction());
        registerVariableFunction("concat",new VariableConcatFunction());
        registerVariableFunction("copy",new VariableCopyFunction());
        registerVariableFunction("get",new VariableGetFunction());
        registerVariableFunction("has",new VariableHasFunction());
        registerVariableFunction("push",new VariablePushFunction());
        registerVariableFunction("remove",new VariableRemoveFunction());
        registerVariableFunction("set",new VariableSetFunction());
        registerVariableFunction("split",new VariableSplitStringFunction());
        registerVariableFunction("substring",new VariableSubStringFunction());
    }

    private void registerBaseGlobalFunction() {
        registerGlobalFunction("print",new PrintfFunction());
        registerGlobalFunction("printf",new PrintfFunction());
        registerGlobalFunction("log",new PrintfFunction());
        registerGlobalFunction("read",new InputFunction());
        registerGlobalFunction("readLine",new ReadLineFunction());
        registerGlobalFunction("sleep",new SleepFunction());
        registerGlobalFunction("time",new TimeFunction());
        registerGlobalFunction("debug",new DebugFunction(this));
    }

    @Override
    public Object execute(String scriptName, String funName, List parameters) {
        ScriptFile scriptFile=getScriptFileByName(scriptName);
        return execute(scriptFile,funName,parameters);
    }
    @Override
    public Object execute(ScriptFile scriptFile, String funName, List parameters) {
        return executeContext.execute(scriptFile,funName,parameters);
    }

    public void registerGlobalFunction(String functionName, FunctionHandler functionHandler){
        globalFunctionMap.put(functionName,functionHandler);
    }
    public void registerVariableFunction(String functionName, VariableFunction variableFunction){
        variableFunctionMap.put(functionName,variableFunction);
    }

    protected VariableFunction getVariableFunction(String functionName){
        return variableFunctionMap.get(functionName);
    }
    protected FunctionHandler getGlobalFunction(String functionName){
        return globalFunctionMap.get(functionName);
    }
    public ScriptFile getScriptFileByName(String name){
        if(context.containsKey(name))
            return context.get(name);
        return null;
    }

    public Result updateScriptFile(String name, CharSequence codeText) {
        return updateScriptFile(name,codeText,true);
    }
	public Result updateScriptFile(String name, CharSequence codeText,boolean clearVariable) {
        ScriptFile scriptFileByPackage = getScriptFileByName(name);
        CodeNode node = grammarTreeHandler.analysis(codeText,lexicalHandler.analysis(codeText));
        scriptFileByPackage.node=node;
        return handlerScriptFile(scriptFileByPackage,clearVariable);
    }
    public Result loadScriptFile(String name, String scriptCode) {
        CodeNode node = grammarTreeHandler.analysis(scriptCode,lexicalHandler.analysis(scriptCode));
        ScriptFile scriptFile =new ScriptFile(node);
        context.put(name,scriptFile);
        return handlerScriptFile(scriptFile,true);
        
    }
    protected Result handlerScriptFile(ScriptFile scriptFile,boolean clearVariable) {
        return executeContext.initScriptFile(scriptFile,clearVariable);
    }

}
