package com.lzt841.script.language.ls;


import com.lzt841.script.grammar.CodeNode;
import com.lzt841.script.language.ls.exception.FunctionNoFoundException;
import com.lzt841.script.language.ls.exception.LsRuntimeException;
import com.lzt841.script.language.ls.execute.CalculateExecute;
import com.lzt841.script.language.ls.execute.ExecuteHandler;
import com.lzt841.script.language.ls.execute.LogicExecute;
import com.lzt841.script.language.ls.function.FunctionHandler;
import com.lzt841.script.language.ls.function.VariableFunction;
import com.lzt841.script.language.ls.model.Result;
import com.lzt841.script.language.ls.model.ScriptFile;
import com.lzt841.script.language.ls.model.Value;
import com.lzt841.script.language.ls.model.Variable;
import com.lzt841.script.language.ls.result.CalculateResult;
import com.lzt841.script.language.ls.utils.CalculateUtils;
import com.lzt841.script.language.ls.utils.KeyWordConfig;
import com.lzt841.script.language.ls.utils.OperatorSymbolConfig;
import com.lzt841.script.language.ls.value.CalculateValue;
import com.lzt841.script.language.ls.value.ValueHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LsExecuteContext {

    public KeyWordConfig keyWordConfig;
    public OperatorSymbolConfig operatorSymbolConfig;
    private LsEnvironment environment;
    private List<ExecuteHandler> executeHandlers =new ArrayList<>();
    private CalculateResult resultHandler;
    private List<ValueHandler> valueHandlers=new ArrayList<>();


    private Variable.VariableOperation variableOperation;
    public LsExecuteContext(LsEnvironment environment) {
       this(environment,new KeyWordConfig(),new OperatorSymbolConfig());
    }
    public LsExecuteContext(LsEnvironment environment,KeyWordConfig keyWordConfig, OperatorSymbolConfig operatorSymbolConfig) {
        this.environment = environment;
        executeHandlers.add(new LogicExecute());//逻辑执行处理
        executeHandlers.add(new CalculateExecute());//执行处理

        resultHandler=new CalculateResult();

        valueHandlers.add(new CalculateValue());//取值处理
        this.operatorSymbolConfig=operatorSymbolConfig;
        this.keyWordConfig=keyWordConfig;
        variableOperation=new LsVariableOperation(this);
    }

    public Variable.VariableOperation getVariableOperation() {
        return variableOperation;
    }

    public void setVariableOperation(Variable.VariableOperation variableOperation) {
        this.variableOperation = variableOperation;
    }

    public List<ValueHandler> getValueHandlers() {
        return valueHandlers;
    }

    public List<ExecuteHandler> getExecuteHandlers() {
        return executeHandlers;
    }

    public CalculateResult getResultHandler() {
        return resultHandler;
    }

    public Object execute(ScriptFile scriptFile, String funName, List parameters) {
        Result result= executeScriptFun(null,0,funName,parameters,scriptFile);
        return result.result;
    }
    public Result initScriptFile( ScriptFile scriptFile,boolean clearVariable){
		if(clearVariable)
        	scriptFile.variableMap.clear();
        List<CodeNode> codeNodes=scriptFile.node.childrens;
		Result result=new Result();
        run(scriptFile,codeNodes,scriptFile.variableMap,0,result);
		return result;
    }

    private void run(ScriptFile scriptFile, List<CodeNode> codeNodes, Map<String,Object> variableMap, int start, Result funResult) {
        int l=codeNodes.size();
        run(scriptFile,codeNodes,variableMap,start,l,funResult);
    }
    public int run(ScriptFile scriptFile, List<CodeNode> codeNodes, Map<String,Object> variableMap, int start, int end, Result funResult) {
        for (; start < end; start++) {
            for (ExecuteHandler executeHandler : executeHandlers) {
                if(executeHandler.canExecute(this,codeNodes,start)){
                    start= executeHandler.execute(this,scriptFile,variableMap,codeNodes,start,funResult);
                    break;
                }
            }
        }
        return start;
    }
    public int getResult(ScriptFile scriptFile, List<CodeNode> codeNodes, Map<String,Object> variableMap, int start, Result result){
                return  resultHandler.calculate(this,scriptFile,variableMap,codeNodes,start,result);
    }

    public Value getValue(ScriptFile scriptFile, Map<String,Object> variableMap, List<CodeNode> codeNodes, int start) {
        for (ValueHandler valueHandler:valueHandlers) {
            if(valueHandler.canGetValue(this ,codeNodes,start)){
                return  valueHandler.getValue(this ,scriptFile,variableMap,codeNodes,start);
            }
        }
        return new Value();
    }
    public int skipExecute( List<CodeNode> codeNodes, int start) {
        for (ExecuteHandler executeHandler : executeHandlers) {
            if(executeHandler.canExecute(this,codeNodes,start)){
                return executeHandler.skipExecute(this,codeNodes,start);
            }
        }
        return start;
    }
    public int skipResult(List<CodeNode> codeNodes, int start) {
        return resultHandler.skipCalculate(this,codeNodes,start);
    }
    public int skipValue( List<CodeNode> codeNodes, int start) {
        for (ValueHandler valueHandler:valueHandlers) {
            if(valueHandler.canGetValue(this ,codeNodes,start)){
                return  valueHandler.skipValue(this ,codeNodes,start);
            }
        }
        return start;
    }
    public Result executeFun(List<CodeNode> nodes, int codeStart, String functionName, List parameters, ScriptFile scriptFile, int type) {
        if(type== Variable.GLOBAL){
            return executeGlobalFun(nodes,codeStart,functionName,parameters);
        }else if(type== Variable.SELF){
            return executeScriptFun(nodes,codeStart,functionName,parameters,scriptFile);
        }else {
            if (scriptFile.variableMap.containsKey(functionName)) {
                return executeScriptFun(nodes,codeStart,functionName,parameters,scriptFile);
            }else {
                return executeGlobalFun(nodes,codeStart,functionName,parameters);
            }
        }
    }

    private Result executeScriptFun(List<CodeNode> nodes, int codeStart, String functionName, List parameters, ScriptFile scriptFile) {
        Object o = scriptFile.variableMap.get(functionName);
        ScriptFile.Function function = null;
        if(ScriptFile.Function.class.isInstance(o))
            function= (ScriptFile.Function) o;
        if(function==null){
            LsRuntimeException.CodeErrorDetail detail=new LsRuntimeException.CodeErrorDetail();
            detail.message="脚本方法 '"+functionName+"' 不存在!";
            if(nodes!=null) {
                CodeNode node = nodes.get(codeStart);
                detail.codeStartIndex = node.getFirstCodeIndex();
                detail.codeEndIndex = node.getEndCodeIndex();
            }
            throw new FunctionNoFoundException(detail);
        }
        return executeScriptFun(function,parameters);
    }
    public Result executeScriptFun(ScriptFile.Function function, List parameters) {
        List<CodeNode> parameter=function.parameterNode.childrens;
        Map<String,Object> variableMap=null;
        Map<String,Object> oldMap=null;
        if(function.variableMap!=null){
            oldMap=new HashMap<>();
            variableMap=function.variableMap;
        }else {
            variableMap=new HashMap<>();
        }
        //设置参数变量值
        int length=parameter.size()-1;
        int size=parameters.size();
        if(parameters!=null) {
            int p = 0;
            for (int i = 1; i < length; i++) {
                int token = parameter.get(i).getFirstWordToken();
                if (keyWordConfig.INTERVAL_KEYWORD.token!=token ) {
                    String key = parameter.get(i).getFirstWordText();
                    if(p < size){
                        Object value = parameters.get(p);
                        if (oldMap != null) {
                            oldMap.put(key, variableMap.get(key));
                        }
                        variableMap.put(key, value);
                        p++;
                    }else {
                        if (oldMap != null) {
                            oldMap.put(key, variableMap.get(key));
                        }
                        variableMap.put(key, null);
                    }
                }
            }
        }
        Result result=new Result();
        run(function.scriptFile,function.funNode.childrens,variableMap,1,result);
        if(oldMap!=null){
            for (String s : oldMap.keySet()) {
                variableMap.put(s,oldMap.get(s));
            }
        }
        return result;
    }
    private Result executeGlobalFun(List<CodeNode> nodes, int codeStart, String functionName, List parameters) {
        FunctionHandler functionHandler = environment.getGlobalFunction(functionName);
        if(functionHandler==null){
            LsRuntimeException.CodeErrorDetail detail=new LsRuntimeException.CodeErrorDetail();
            detail.message="全局方法 '"+functionName+"' 不存在!";
            CodeNode node = nodes.get(codeStart);
            detail.codeStartIndex=node.getFirstCodeIndex();
            detail.codeEndIndex=node.getEndCodeIndex();
            throw new FunctionNoFoundException(detail);
        }
        try {
            Object result= functionHandler.execute(parameters);
            return new Result(result);
        }catch (LsRuntimeException exception){
            LsRuntimeException.CodeErrorDetail detail=new LsRuntimeException.CodeErrorDetail();
            detail.message="全局方法 '"+functionName+"' !";
            CodeNode node = nodes.get(codeStart);
            detail.codeStartIndex=node.getFirstCodeIndex();
            detail.codeEndIndex=node.getEndCodeIndex();
            throw new FunctionNoFoundException(detail);
        }

    }

    public VariableFunction getVariableFunction(String funName){
        VariableFunction variableFunction = environment.getVariableFunction(funName);
        return variableFunction;
    }
    //执行变量方法
    public Result executeVariableFun(List<CodeNode> nodes, int codeStart, Object variable, String funName, List parameters) {
        VariableFunction variableFunction=getVariableFunction(funName);
        if(variableFunction==null){
            LsRuntimeException.CodeErrorDetail detail=new LsRuntimeException.CodeErrorDetail();
            detail.message="变量方法 '"+funName+"' 不存在!";
            CodeNode node = nodes.get(codeStart);
            detail.codeStartIndex=node.getFirstCodeIndex();
            detail.codeEndIndex=node.getEndCodeIndex();
            throw new FunctionNoFoundException(detail);
        }
        return executeVariableFun(variable,variableFunction,parameters);
    }
    //执行变量方法
    public Result executeVariableFun(Object variable, VariableFunction variableFunction, List parameters) {
        Object result = variableFunction.execute(variable, parameters);
        return new Result(result);
    }

    public boolean hasGlobalVariable(String name){
        return environment.globalVariableMap.containsKey(name);
    }
    public Map<String,Object> getGlobalVariable(){
        return environment.globalVariableMap;
    }



}
