package com.lzt841.script.language.ls.function;

import java.util.List;

public interface FunctionHandler {
    Object execute(List parameters);
}
