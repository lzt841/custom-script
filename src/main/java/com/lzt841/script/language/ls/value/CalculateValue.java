package com.lzt841.script.language.ls.value;

import com.lzt841.script.grammar.CodeNode;
import com.lzt841.script.language.ls.LsExecuteContext;
import com.lzt841.script.language.ls.model.Result;
import com.lzt841.script.language.ls.model.ScriptFile;
import com.lzt841.script.language.ls.model.Value;
import com.lzt841.script.language.ls.model.Variable;
import com.lzt841.script.language.ls.utils.CalculateUtils;
import com.lzt841.script.language.ls.utils.NodeUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateValue implements ValueHandler {
    @Override
    public boolean canGetValue(LsExecuteContext context, List<CodeNode> codeNodes, int start) {
        return true;
    }


    public Value getValue(LsExecuteContext context, ScriptFile scriptFile, Map<String, Object> variableMap, List<CodeNode> codeNodes, int start){
        CodeNode node = codeNodes.get(start);
        String word=node.getFirstWordText();
        int token=node.getFirstWordToken();

        Value calculate=new Value();
        Variable variable = null;
		if(token== context.keyWordConfig.STRING1_START_KEYWORD.token||token== context.keyWordConfig.STRING2_START_KEYWORD.token){//字符串
            calculate.value= word;
        } else if(context.keyWordConfig.ARRAY_START_KEYWORD.token==token){//数组
            Result result1 = new Result();
            List list = new ArrayList();
            for (int i = 1; i < node.childrens.size() - 1; i += 2) {
                result1.result = null;
                i = context.getResult(scriptFile, node.childrens, variableMap, i, result1);
                list.add(result1.result);
            }
            calculate.value = list;

        }else if(context.keyWordConfig.OBJECT_START_KEYWORD.token==token) {//hash对象

            Result result1 = new Result();
            Map map = new HashMap();
            for (int i = 1; i < node.childrens.size() - 1; i += 2) {
                result1.result = null;
                String name = node.childrens.get(i).getFirstWordText();
                i = context.getResult(scriptFile, node.childrens, variableMap, i + 2, result1);
                map.put(name, result1.result);
            }
            calculate.value = map;

        } else if(context.operatorSymbolConfig.INCREMENTAL.token==token){//++a
            variable = CalculateUtils.findVariableRight(context, scriptFile, variableMap, codeNodes, start + 1);
            start=variable.end;
            context.getVariableOperation().setVariableValue(variable,CalculateUtils.toInteger(context.getVariableOperation().getVariableValue(variable)) + 1);
            calculate.value = context.getVariableOperation().getVariableValue(variable);
        }else if(context.operatorSymbolConfig.DECREMENT.token==token){//--a
            variable = CalculateUtils. findVariableRight(context, scriptFile, variableMap, codeNodes, start+1 );
            start=variable.end;
            context.getVariableOperation().setVariableValue(variable,CalculateUtils.toInteger(context.getVariableOperation().getVariableValue(variable))-1);
            calculate.value=context.getVariableOperation().getVariableValue(variable);
        }else if(context.keyWordConfig.TRUE_KEYWORD.token==token|| context.keyWordConfig.FALSE_KEYWORD.token==token){//boolean值
            calculate.value=Boolean.parseBoolean(word);
        }else if(context.keyWordConfig.NULL_KEYWORD.token==token){//空值
            calculate.value=null;
        } else if (context.keyWordConfig.PARAMETER_START_KEYWORD.token==token) {//()
            if (start+2<codeNodes.size()
                    &&context.keyWordConfig.FUNCTION_ARROW_KEYWORD.token==codeNodes.get(start+1).getFirstWordToken()
                    &&context.keyWordConfig.EXECUTOR_START_KEYWORD.token==codeNodes.get(start+2).getFirstWordToken()) {//()=>{}匿名函数
                ScriptFile.Function function=new ScriptFile.Function();
                function.parameterNode=node;
                function.scriptFile=scriptFile;
                function.funNode=codeNodes.get(start+2);
                function.variableMap=variableMap;
                start+=2;
                calculate.value = function;
            }else {//()值
                Result result1=new Result();
                context.getResult(scriptFile, node.childrens, variableMap, 1, result1);
                calculate.value = result1.result;
            }
        } else if(context.operatorSymbolConfig.NOT.token==token){//!aaa
            start++;
            Value calculate1 = context.getValue( scriptFile, variableMap, codeNodes, start);

            calculate.value=!CalculateUtils.toBoolean(calculate1.value);
            variable=calculate1.variable;
            start=calculate1.start;
        } else if (context.keyWordConfig.NUMBER_KEYWORD.token==token){//数字
            calculate.value=node.word.data;
        } else if(context.operatorSymbolConfig.MINUS.token==token|| context.operatorSymbolConfig.ADDITIVE.token==token){//有符号数字或变量
            start++;
            Value calculate1 = context.getValue( scriptFile, variableMap, codeNodes, start);
            if(context.operatorSymbolConfig.MINUS.token==token){
                if(CalculateUtils.isDouble(calculate1.value)){
                    calculate1.value=-CalculateUtils.toDouble(calculate1.value);
                }else if(CalculateUtils.isFloat(calculate1.value)){
                    calculate1.value=-CalculateUtils.toFloat(calculate1.value);
                }else if(CalculateUtils.isLong(calculate1.value)){
                    calculate1.value=-CalculateUtils.toLong(calculate1.value);
                }else {
                    calculate1.value=-CalculateUtils.toInteger(calculate1.value);
                }
            }
            calculate.value=calculate1.value;
            variable=calculate1.variable;
            start=calculate1.start;
        } else if (context.keyWordConfig.FUNCTION_KEYWORD.token==token&&start+2<codeNodes.size()) {//方法函数
            if(codeNodes.get(start+1).getFirstWordToken()==context.keyWordConfig.PARAMETER_START_KEYWORD.token){//匿名方法
                ScriptFile.Function function=new ScriptFile.Function();
                function.parameterNode=codeNodes.get(start+1);
                function.scriptFile=scriptFile;
                function.funNode=codeNodes.get(start+2);
                function.variableMap=variableMap;
                calculate.value = function;
                start+=2;
            }else {//非匿名函数
                ScriptFile.Function function=new ScriptFile.Function();
                function.parameterNode=codeNodes.get(start+2);
                function.scriptFile=scriptFile;
                function.funNode=codeNodes.get(start+3);
                calculate.value = function;
                scriptFile.variableMap.put(codeNodes.get(start+1).getFirstWordText(),function);
                start+=3;
            }
        } else{//变量
                    variable = CalculateUtils.findVariableRight(context, scriptFile, variableMap, codeNodes, start);
                start=variable.end;
                if(start+1<codeNodes.size()){
                    int nextToken=codeNodes.get(start+1).getFirstWordToken();
                    if(context.operatorSymbolConfig.INCREMENTAL.token==nextToken|| context.operatorSymbolConfig.DECREMENT.token==nextToken){//a++或a--情况
                        calculate.value=context.getVariableOperation().getVariableValue(variable);//先赋值,再运算
                        context.getVariableOperation().setVariableValue(variable,variable.getValueAsInt(context)+(context.operatorSymbolConfig.INCREMENTAL.token==nextToken?1:-1));
                        start+=1;
                    }else {
                        calculate.value=context.getVariableOperation().getVariableValue(variable);
                    }
                }else {
                    calculate.value = context.getVariableOperation().getVariableValue(variable);
                }
            }

        if(variable==null){
            variable=new Variable();
            variable.noKey=true;
            variable.library=calculate.value;
        }
        calculate.start =start;
        calculate.variable=variable;
        return calculate;
    }


    @Override
    public int skipValue(LsExecuteContext context, List<CodeNode> codeNodes, int start) {
        CodeNode node = codeNodes.get(start);

        int token=node.getFirstWordToken();
       if(context.operatorSymbolConfig.INCREMENTAL.token==token){//(++a)
            start = CalculateUtils. findVariableRightIndex(context, codeNodes, start+1 );
        }else if(context.operatorSymbolConfig.DECREMENT.token==token){//(--a)
            start = CalculateUtils. findVariableRightIndex(context, codeNodes, start+1 );
        }else if(context.operatorSymbolConfig.NOT.token==token){// !a
            start++;
            start= context.skipValue(codeNodes,start);
        } else if(context.operatorSymbolConfig.MINUS.token==token|| context.operatorSymbolConfig.ADDITIVE.token==token){//有正负符号
            start++;
            start= context.skipValue(codeNodes,start);
        }else if (context.keyWordConfig.FUNCTION_KEYWORD.token == token) {//方法体跳过
            start+=NodeUtils.findNodeByToken(codeNodes,context.keyWordConfig.EXECUTOR_START_KEYWORD.token,start);
       } if (start+2<codeNodes.size()
                &&context.keyWordConfig.PARAMETER_START_KEYWORD.token==token
                &&context.keyWordConfig.FUNCTION_ARROW_KEYWORD.token==codeNodes.get(start+1).getFirstWordToken()
                &&context.keyWordConfig.EXECUTOR_START_KEYWORD.token==codeNodes.get(start+2).getFirstWordToken()) {//匿名函数
                start+=2;
        }
            else if(context.keyWordConfig.GLOBAL_KEYWORD.token==token
               || context.keyWordConfig.SELF_KEYWORD.token==token
               ||context.keyWordConfig.DEFAULT_KEYWORD.token==token) {//变量或者函数
                start = CalculateUtils.findVariableRightIndex(context,codeNodes, start);
                if (start + 1 < codeNodes.size()) {
                    int nextToken = codeNodes.get(start + 1).getFirstWordToken();
                    if (context.operatorSymbolConfig.INCREMENTAL.token==nextToken || context.operatorSymbolConfig.DECREMENT.token==nextToken) {//a++或a--情况
                        start += 1;
                    }
                }
            }

        return start;
    }
}
