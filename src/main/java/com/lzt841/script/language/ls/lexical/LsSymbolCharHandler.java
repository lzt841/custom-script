package com.lzt841.script.language.ls.lexical;

import com.lzt841.script.language.ls.model.Keyword;
import com.lzt841.script.language.ls.model.Operator;
import com.lzt841.script.lexical.SymbolCharHandler;

public class LsSymbolCharHandler extends SymbolCharHandler {
    public LsSymbolCharHandler(Keyword keyword) {
        this(keyword, false);
    }
    public LsSymbolCharHandler(Keyword keyword, boolean lose) {
        super(keyword.keyword, lose, keyword.token);
    }
    public LsSymbolCharHandler(Operator operator) {
        this(operator, false);
    }

    public LsSymbolCharHandler(Operator operator, boolean lose) {
        super(operator.operator, lose, operator.token);
    }

    public LsSymbolCharHandler(String symbol, int token) {
        super(symbol, token);
    }

    public LsSymbolCharHandler(String symbol, boolean lose, int token) {
        super(symbol, lose, token);
    }
}
