package com.lzt841.script.language.ls.model;

public class Operator {
    public String operator;
    public int operatorLevel=-8;

    public int token;

    public Operator(String operator, int operatorLevel,int token) {
        this.operator = operator;
        this.operatorLevel = operatorLevel;
        this.token=token;
    }

    @Override
    public boolean equals(Object obj) {
        if(operator==obj)return true;
        if(operator==null)return false;
        return operator.equals(obj);
    }

    @Override
    public String toString() {
        return operator +" " +operatorLevel;
    }
}
