package com.lzt841.script.language.ls.model;


import com.lzt841.script.language.ls.LsExecuteContext;
import com.lzt841.script.language.ls.utils.CalculateUtils;

import java.util.List;
import java.util.Map;

public class Variable{
    public static final int DEFAULT=0,GLOBAL=1,SELF=2;
    public Object library;
    public Object key;
    public int end;
    public boolean noKey=false;
    public int type=DEFAULT;
    
    public int getValueAsInt(LsExecuteContext context){
        Object v=context.getVariableOperation().getVariableValue(this);
        return CalculateUtils.toInteger(v);
    }
    public  interface VariableOperation{
         void setVariableValue(Variable variable, Object value);
        Object getVariableValue(Variable variable);
        boolean isExistence(Variable variable);
    }
}
