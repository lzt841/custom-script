package com.lzt841.script.language.ls.exception;

public class LsRuntimeException extends RuntimeException{

    private CodeErrorDetail detail;

    public LsRuntimeException(CodeErrorDetail detail) {
        super(detail.message);
        this.detail = detail;
    }

    public CodeErrorDetail getDetail() {
        return detail;
    }

    @Override
    public String toString() {
        return getDetail().toString();
    }

    public static class CodeErrorDetail{
        public String message;
        public int errorCode;
        public int codeStartIndex;
        public int codeEndIndex;

        public CodeErrorDetail() {
        }

        public CodeErrorDetail(String message, int errorCode, int codeStartIndex, int codeEndIndex) {
            this.message = message;
            this.errorCode = errorCode;
            this.codeStartIndex = codeStartIndex;
            this.codeEndIndex = codeEndIndex;
        }

        @Override
        public String toString() {
            return "CodeErrorDetail{" +
                    "message='" + message + '\'' +
                    ", errorCode='" + errorCode + '\'' +
                    ", codeStartIndex=" + codeStartIndex +
                    ", codeEndIndex=" + codeEndIndex +
                    '}';
        }
    }
}
