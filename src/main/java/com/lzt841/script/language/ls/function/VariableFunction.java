package com.lzt841.script.language.ls.function;

import java.util.List;

public interface VariableFunction {
    Object execute(Object variable,List parameters);
}
