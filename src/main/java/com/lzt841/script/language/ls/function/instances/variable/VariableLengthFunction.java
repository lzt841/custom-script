package com.lzt841.script.language.ls.function.instances.variable;

import com.lzt841.script.language.ls.function.VariableFunction;

import java.util.List;
import java.util.Map;

public class VariableLengthFunction implements VariableFunction {
    @Override
    public Object execute(Object variable, List parameters) {
        if (List.class.isInstance(variable)) {
            return ((List)variable).size();
        } else if (Map.class.isInstance(variable)) {
            return ((Map)variable).size();
        }else if(CharSequence.class.isInstance(variable)){
            return ((CharSequence) variable).length();
        }
        return null;
    }
}
