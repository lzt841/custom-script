package com.lzt841.script.language.ls.function.instances.variable;

import com.lzt841.script.language.ls.function.VariableFunction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VariableCopyFunction implements VariableFunction {
    @Override
    public Object execute(Object variable, List parameters) {
        if (Map.class.isInstance(variable)) {
            return copyMap((Map) variable);
        }else if (List.class.isInstance(variable)) {
            return copyList((List) variable);
        }
        return variable;
    }

    private Object copyList(List variable) {
        ArrayList<Object> result = new ArrayList<>();
        Collections.copy(result,variable);
        return result;
    }

    private Map copyMap(Map variable) {
       Map result=new HashMap<>();
        for (Object o : variable.keySet()) {
            result.put(o,variable.get(o));
        }
        return result;
    }
}
