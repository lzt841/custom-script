package com.lzt841.script.lexical;

import java.util.List;

public class SymbolCharHandler implements CharHandler {

    @Override
    public int getSort() {
        return symbol.length;
    }
    

    protected  char symbol[];
    protected int start;
    protected boolean lose=false;
    protected int token;
    public SymbolCharHandler(char symbol[]) {
        this(symbol,0);
    }
    public SymbolCharHandler(char symbol[],boolean lose) {
        this(symbol,lose,0);
    }

    public SymbolCharHandler(String symbol) {
        this(symbol.toCharArray(),0);
    }
    public SymbolCharHandler(String symbol,int token) {
        this(symbol.toCharArray(),token);
    }
    public SymbolCharHandler(String symbol,boolean lose,int token) {
        this(symbol.toCharArray(),lose,token);
        this.token=token;
    }
    public SymbolCharHandler(char symbol[],int token) {
        this(symbol, false,token);
    }
    public SymbolCharHandler(char symbol[], boolean lose,int token) {
        this.symbol = symbol;
        this.lose = lose;
        this.token=token;
    }

    @Override
    public boolean tryEnter(CharSequence text, int index) {
        int l=text.length();
        if (symbol.length + index - 1 >= l)return false;
        for (int i=symbol.length - 1;i >= 0;i--) {
            if (symbol[i] != text.charAt(i + index))
                return false;
        }
        return true;
    }
    @Override
    public void enter(CharSequence text, int index) {
        start = index;
    }

    @Override
    public boolean tryExit(CharSequence text, int index) {
        return index - start >= symbol.length;
    }


    public void exit(CharSequence text, int index, List<Word> wordsOut) {
        if (!lose) {
            Word w=new Word();
            w.word = text.subSequence(start, index).toString();
            w.start = start;
            w.end = index;
            w.token=token;
            wordsOut.add(w);
        }
    }






}
