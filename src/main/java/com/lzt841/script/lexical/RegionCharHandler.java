package com.lzt841.script.lexical;

public class RegionCharHandler extends SymbolCharHandler {
    
   protected char endSymbol[];
    public RegionCharHandler(String symbol,int token) {
        this(symbol,symbol,token);
    }
    public RegionCharHandler(String startSymbol,String endSymbol,int token) {
        this(startSymbol.toCharArray(),endSymbol.toCharArray(),false,token);
    }
    public RegionCharHandler(String startSymbol,String endSymbol,boolean lose,int token) {
        this(startSymbol.toCharArray(),endSymbol.toCharArray(),lose,token);
    }
    public RegionCharHandler(char symbol[],int token) {
        this(symbol,false,token);
    }
    public RegionCharHandler(char symbol[],boolean lose,int token) {
        this(symbol,symbol,lose,token);
    }
    public RegionCharHandler(char startSymbol[],char endSymbol[]) {
        this(startSymbol,endSymbol,0);
    }
    public RegionCharHandler(char startSymbol[],char endSymbol[],int token) {
        this(startSymbol,endSymbol,false,token);
    }

    public RegionCharHandler(char startSymbol[],char endSymbol[],boolean lose,int token) {
        super(startSymbol,lose,token);
        this.endSymbol=endSymbol;
        
    }
    @Override
    public boolean tryExit(CharSequence text, int index) {
        int s=symbol.length;
        int e=endSymbol.length;
        if (index-start>=s+e){
            while(--index>=0&&--e>=0){
                if(endSymbol[e]!=text.charAt(index))
                    return false;
            }
            return true;
       }
          return false;
    }
    
    
}
