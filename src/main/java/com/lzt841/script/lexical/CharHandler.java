package com.lzt841.script.lexical;

import java.util.List;


public interface CharHandler 
{
  
    boolean tryEnter(CharSequence text, int index);
    void enter(CharSequence text, int index);
    
    boolean tryExit(CharSequence text,int index);
    void exit(CharSequence text, int index,List<Word> wordsOut) ;
    
    int getSort();
}
