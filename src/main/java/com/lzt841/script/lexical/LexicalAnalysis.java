package com.lzt841.script.lexical;

import java.util.*;

public class LexicalAnalysis implements LexicalHandler {
    protected List<CharHandler> handlers=new ArrayList<>();

    protected Map<String,Integer> keywords=new HashMap<>();
    public LexicalAnalysis(boolean loadDefault){
        if(loadDefault)
            handlers=initDefault();
    }

    private List<CharHandler> initDefault() {
        
        CharHandler[] handler=new CharHandler[]{
            
            new SymbolCharHandler(new char[]{'>','>'}),
            new SymbolCharHandler(new char[]{'<','<'}),
            new SymbolCharHandler(new char[]{'&','&'}),
            new SymbolCharHandler(new char[]{'|','|'}),
            new SymbolCharHandler(new char[]{'!','='}),
            new SymbolCharHandler(new char[]{'=','='}),
            new SymbolCharHandler(new char[]{'>','='}),
            new SymbolCharHandler(new char[]{'<','='}),
            new SymbolCharHandler(new char[]{'+','='}),
            new SymbolCharHandler(new char[]{'-','='}),
            new SymbolCharHandler(new char[]{'*','='}),
            new SymbolCharHandler(new char[]{'/','='}),
            new SymbolCharHandler(new char[]{'%','='}),
            new SymbolCharHandler(new char[]{'^','='}),
            new SymbolCharHandler(new char[]{'&','='}),
            new SymbolCharHandler(new char[]{'|','='}),
            new SymbolCharHandler(new char[]{'+','+'}),
            new SymbolCharHandler(new char[]{'-','-'}),

            new RegionCharHandler(new char[]{'/','/'},new char[]{'\n'}),
            new RegionCharHandler(new char[]{'/','*'},new char[]{'*','/'}),
            
            new SymbolCharHandler(new char[]{' '}, true),
            new SymbolCharHandler(new char[]{'\f'}, true),
            new SymbolCharHandler(new char[]{'\n'}, true),
            new SymbolCharHandler(new char[]{'\t'}, true),
            new SymbolCharHandler(new char[]{'\r'}, true),
            
            new StringCharHandler(new char[]{'\''}),
            new StringCharHandler(new char[]{'\"'}),
            
            new SymbolCharHandler(new char[]{'='}),
            new SymbolCharHandler(new char[]{','}),
            new SymbolCharHandler(new char[]{'.'}),
            new SymbolCharHandler(new char[]{'?'}),
            new SymbolCharHandler(new char[]{'!'}),
            new SymbolCharHandler(new char[]{'@'}),
            new SymbolCharHandler(new char[]{'#'}),
            new SymbolCharHandler(new char[]{'%'}),
            new SymbolCharHandler(new char[]{'^'}),
            new SymbolCharHandler(new char[]{'&'}),
            new SymbolCharHandler(new char[]{'*'}),
            new SymbolCharHandler(new char[]{'/'}),
            new SymbolCharHandler(new char[]{'\\'}),
            new SymbolCharHandler(new char[]{'~'}),
            new SymbolCharHandler(new char[]{'('}),
            new SymbolCharHandler(new char[]{')'}),
            new SymbolCharHandler(new char[]{'['}),
            new SymbolCharHandler(new char[]{']'}),
            new SymbolCharHandler(new char[]{'{'}),
            new SymbolCharHandler(new char[]{'}'}),
            new SymbolCharHandler(new char[]{'<'}),
            new SymbolCharHandler(new char[]{'>'}),
            new SymbolCharHandler(new char[]{'+'}),
            new SymbolCharHandler(new char[]{'-'}),
            new SymbolCharHandler(new char[]{'|'}),
            new SymbolCharHandler(new char[]{';'}),
            new SymbolCharHandler(new char[]{':'}),

            new DefaultCharHandler()
        };
        List<CharHandler> ha= Arrays.asList(handler);
		Collections.sort(handlers, new Comparator<CharHandler>(){
                @Override
                public int compare(CharHandler o1, CharHandler o2) {
                    return o2.getSort()-o1.getSort();
                }
            });
        return ha;
    }
    public void addCharHandler(CharHandler handler){
		List<CharHandler> newHandler=new ArrayList<>();
		newHandler.addAll(handlers);
		newHandler.add(handler);
		handlers=newHandler;
		Collections.sort(handlers, new Comparator<CharHandler>(){
                @Override
                public int compare(CharHandler o1, CharHandler o2) {
                    return o2.getSort()-o1.getSort();
                }
            });
	}
    public List<CharHandler> getHandler(){
        return handlers;
    }

    public Map<String, Integer> getKeywords() {
        return keywords;
    }

    @Override
    public List<Word> analysis(CharSequence codeText){
        List<Word> out=new ArrayList<>();
        return analysis(codeText,out);
    }
    @Override
    public List<Word> analysis(CharSequence codeText, List<Word> out){
        int length=codeText.length();
        CharHandler now=null;
        for (int i=0;i < length;i++) {
            if (now == null || now.tryExit(codeText, i)) {
                for (CharHandler h:getHandler()) {
                    if(h instanceof DefaultCharHandler && now instanceof DefaultCharHandler){
                        break;
                    }
                    
                    if (h.tryEnter(codeText, i)) {
                        if (now != null) {
                            now.exit(codeText, i, out);
                        }
                        now = h;
                        now.enter(codeText,i);
                        break;
                    }
                }
            }
        }
        if(now!=null){
            now.exit(codeText,length,out);
        }
        Set<String> keys = keywords.keySet();
        for (String key : keys) {
            for (Word word : out) {
                if(key.equals(word.word)){
                    word.token=keywords.get(key);
                }
            }
        }
        return out;
        
        }
    
}
