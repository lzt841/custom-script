package com.lzt841.script.lexical;

public class StringCharHandler extends RegionCharHandler {
    public StringCharHandler(String symbol,int token) {
        this(symbol,symbol,token);
    }
    public StringCharHandler(char symbol[]) {
        this(symbol,symbol,false,0);
    }
    public StringCharHandler(String startSymbol,String endSymbol,int token) {
        this(startSymbol.toCharArray(),endSymbol.toCharArray(),false,token);
    }
    public StringCharHandler(String startSymbol,String endSymbol,boolean lose,int token) {
        this(startSymbol.toCharArray(),endSymbol.toCharArray(),lose,token);
    }

    public StringCharHandler(char startSymbol[],char endSymbol[],boolean lose,int token) {
        super(startSymbol,endSymbol,lose,token);
    }


    @Override
    public boolean tryExit(CharSequence text, int index) {

        if (super.tryExit(text, index)) {
            int e=endSymbol.length;
            index -= e;
            int num=0;
            while (--index >= 0) {
                if (text.charAt(index) == '\\') {
                    num++;
                } else {
                    break;
                }
            }
            return (num & 1) == 0;
        }
        return false;
    }


}
