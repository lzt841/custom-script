package com.lzt841.script.lexical;

import java.util.List;

public class DefaultCharHandler implements CharHandler
{

    @Override
    public int getSort() {
        return -1;
    }
    

   
    
    protected int start;
    
    @Override
    public boolean tryEnter(CharSequence text, int index) {
        return true;
    }
    @Override
    public void enter(CharSequence text, int index) {
        start=index;
        
    }

    @Override
    public boolean tryExit(CharSequence text, int index) {
        return true;
    }
    
    @Override
    public void exit(CharSequence text, int index,List<Word> wordsOut) {
        Word w=new Word();
        w.word=text.subSequence(start,index).toString();
        w.start=start;
        w.end=index;
        wordsOut.add(w);
        
    }
    

    
    
  
    
}
