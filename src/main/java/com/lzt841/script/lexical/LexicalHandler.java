package com.lzt841.script.lexical;

import java.util.List;

public interface LexicalHandler {
    List<Word> analysis(CharSequence codeText);
   List<Word> analysis(CharSequence codeText, List<Word> out);
   
}
