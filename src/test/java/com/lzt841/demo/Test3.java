package com.lzt841.demo;

import com.lzt841.script.language.ls.*;
import com.lzt841.script.language.ls.function.*;

import java.util.*;
import com.lzt841.script.language.ls.model.*;
import com.lzt841.script.grammar.*;
import com.lzt841.script.language.ls.value.ValueHandler;

public class Test3
{

	/*
	 *实现增加变量基本类型
	 *描述：增加char字符基本类型,语言默认'和"都是字符串的，现在将'当做字符类型
	 *示例: a='a'
	 */
	public static void main(String[] args){
		String scriptName="test";

		//测试用例代码脚本
		String code=
			"function main(){"
			+"a=123*23 \n"
			+"b=45\n"
		
			+"x='a'"
			+"y='字符串'"
			+"print('x的类型:'+x.type()+'\n')"
			+"print('y的类型:'+y.type()+'\n')"
			+"}";


		LsEnvironment scripts=new LsEnvironment();
		
		//实例化值操作接口
		ValueHandler charValueHandler=new ValueHandler(){
			@Override
			public boolean canGetValue(LsExecuteContext context, List<CodeNode> codeNodes, int start)
			{
				// 返回能否处理这个节点为值，我们要处理''的节点，就在节点token为'的token时返回true
				CodeNode node= codeNodes.get(start);
				int token=node.getFirstWordToken();
				//token为‘的token时返回true
				return token== context.keyWordConfig.STRING1_START_KEYWORD.token;
			}

			@Override
			public Value getValue(LsExecuteContext context, ScriptFile scriptFile, Map<String, Object> variableMap, List<CodeNode> codeNodes, int start)
			{
				//在这里返回节点的值，我们让''的长度为1时就返回char类型，长度不为1时就反会字符串类型
				Value value=new Value();
				value.start=start;//这个指针要设置上去
				CodeNode node=codeNodes.get(start);
				//拿到脚本节点的原文本
				String text=node.getFirstWordText();
				if(text.length()!=1){//长度不为，那就是字符串
					value.value=text;
				}else{
					//长度为1时就返回一个字符
					value.value=text.charAt(0);
				}
				return value;
			}

			@Override
			public int skipValue(LsExecuteContext context, List<CodeNode> codeNodes, int start)
			{
				// 返回处理的新的指针，没有处理，直接返回原位置
				return start;
			}
		};
		
		//将我们的值处理程序放到处理程序序列的前面，优先处理
		scripts.getExecuteContext().getValueHandlers().add(0,charValueHandler);
		
		
		
		
		
		//套用Test2示例
		//注册变量操作方法type(),返回变量的类型字符串
		scripts.registerVariableFunction("type", new VariableFunction(){
				@Override
				public Object execute(Object variable, List parameters)
				{
					if(Character.class.isInstance(variable)){
						return "char";
					}else if(Map.class.isInstance(variable)){
						return "map";
					}else if(List.class.isInstance(variable)){
						return "list";
					}else if(Integer.class.isInstance(variable)){
						return "int";
					}else if(Float.class.isInstance(variable)){
						return "float";
					}else if(Boolean.class.isInstance(variable)){	
						return "boolean";
					}else if(CharSequence.class.isInstance(variable)){
						return "string";
					}else{
						return "other";
					}
				}
			});
		//加载脚本
		scripts.loadScriptFile(scriptName,code);


		//运行脚本方法
		Object result= scripts.execute(scriptName,"main",new ArrayList());

		
		
	}
	
	
}
