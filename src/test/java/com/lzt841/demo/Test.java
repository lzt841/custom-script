package com.lzt841.demo;

import com.lzt841.script.language.ls.*;
import java.util.*;

import com.lzt841.script.language.ls.execute.ExecuteHandler;
import com.lzt841.script.language.ls.utils.*;
import com.lzt841.script.language.ls.lexical.*;
import com.lzt841.script.language.ls.model.*;
import com.lzt841.script.grammar.*;

public class Test
{
	/*
	*实现自定义执行命令，自定义关键词，操作运算符
	*描述：将#当做输出打印语句
	*示例: #'打印内容'
	*/
	public static void main(String[] args)
	{
		KeyWordConfig keyWordConfig=new KeyWordConfig();
		OperatorSymbolConfig operatorSymbolConfig=new OperatorSymbolConfig();
		String scriptName="test";
		//将方法定义关键词修改为中文“方法“,修改后对已加载的脚本无影响，大部分关键词都可以修改，只要注意不建议修改token，还有不要词重复重叠就行
		keyWordConfig.FUNCTION_KEYWORD.keyword="方法";
		//运算符和关键词一样，可以修改，token不建议改
		operatorSymbolConfig.EQUAL.operator="等于";
		//测试用例代码脚本
		String code="方法 main(){"
						+"#'开始自定义命令打印了' \n"
						+"a=true \n"
						+"if (a 等于 true)\n"
						+	"#'打印成功' \n"
						+"else "
						+"#'打印错误'\n"
						+"a='我是变量\n'\n"
						+"//测试打印变量\n"
						+"#a   \n"
						+"//测试打印公式\n"
						+"#a+',我是拼接字符串,'+(12+32)/4/6.0f"
						+"}";

		LsEnvironment scripts=new LsEnvironment(keyWordConfig,operatorSymbolConfig);

		//第一步:获取词法分析器，注入一个#符合的词；给这个#一个token，这个token要唯一，随便取，取200以外好，要保证唯一就行，例子里就2000好了
		LsLexicalAnalysis analysis= scripts.getLexicalHandler();
		final int token=2000;
		//添加词处理程序进去
		analysis.addCharHandler(new LsSymbolCharHandler(new Keyword("#",token)));
		
		//第二步，写一个注入处理这个#词的命令的类
		ExecuteHandler execute=new ExecuteHandler(){
			//这个方法是做处理命令的判断，返回true代表能处理，我们只处理#的命令，所以当命令为#时返回true就好
			@Override
			public boolean canExecute(LsExecuteContext context, List<CodeNode> codeNodes, int i)
			{
				//拿到当前位置的节点（i是指当前节点位置）
				CodeNode node=codeNodes.get(i);
				//取出节点的token
				int nodeToken=node.getFirstWordToken();
				//这个token和我们刚才定义的#命令的token一样就返回true就好了
				return nodeToken==token;
			}
			//这个方法是具体的执行操作方法
			@Override
			public int execute(LsExecuteContext context, ScriptFile scriptFile, Map<String, Object> variableMap, List<CodeNode> codeNodes, int i, Result funResult)
			{
				//已知道i是#节点的位置，我们要拿到#后面的结果，可用executeHandle的getResult方法得到后面的计算结果，
				//如果不想要计算结果，可以用getValue，只获取一个值，不对之后是否有运算符都忽略掉了
				//创建一个空结果对象去接收结果，方法返回的是指针往下走的位置，一般都是跟着指针走的（逻辑分支才根据逻辑走），所以直接让i跳到指针位置
				Result result=new Result();
				i= context.getResult(scriptFile,codeNodes,variableMap,i+1,result);
				//拿到结果后怎么办？#命令是用来打印的，那当然是用起来
				System.out.println(result.result);
				
				//完成，将新的指针位置返回给处理程序
				return i;
			}
			//跳过指令
			@Override
			public int skipExecute(LsExecuteContext context, List<CodeNode> codeNodes, int i) {
				//指令需要从后面拿一个结果,所以返回跳过下一个结果的指针
				return context.skipResult(codeNodes,i+1);
			}
		};
		//将我们的命令处理程序放到处理程序序列的前面，优先处理
		 scripts.getExecuteContext().getExecuteHandlers().add(0,execute);
		
		
		//加载脚本
		scripts.loadScriptFile(scriptName,code);
		
		
		//运行脚本方法
		Object result= scripts.execute(scriptName,"main",new ArrayList());
		

		//更复杂的命令都是同理
	
	}
	
	
}
