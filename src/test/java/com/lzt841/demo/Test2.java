package com.lzt841.demo;

import com.lzt841.script.grammar.*;
import com.lzt841.script.language.ls.*;
import com.lzt841.script.language.ls.lexical.*;
import com.lzt841.script.language.ls.model.*;
import com.lzt841.script.language.ls.utils.*;
import java.util.*;
import com.lzt841.script.language.ls.function.*;

public class Test2
{
	/*
	 *实现自定义全局方法,变量操作方法
	 *描述：脚本调用全局方法
	 *示例: 1. global.min(1,2) 
	 *     2. min(1,2)
	 *	   3. x.type()
	 */
	public static void main(String[] args)
	{
		String scriptName="test";
		
		//测试用例代码脚本
		String code=
		"function main(){"
			+"a=123*23 \n"
			+"b=45\n"
			+"//用self关键词强行指定方法为脚本方法\n"
			+"//self.min(a,b)\n"
			+"if (min(a,b)==a){\n"
			+	"printf('a是最小值\n') \n"
			+"}else{ "
			+"printf('b是最小值\n')"
			+"\n}"
			+"x={}"
			+"print('x的类型:'+x.type())"
		+"}";


		LsEnvironment scripts=new LsEnvironment();
		//注册全局方法min，求最小值
		scripts.registerGlobalFunction("min", new FunctionHandler(){
				@Override
				public Object execute(List parameters)
				{
					if(parameters.size()==0)return 0;
					if(parameters.size()==1)return parameters.get(0);
					return Math.min(CalculateUtils.toInteger(parameters.get(0)),CalculateUtils.toInteger(parameters.get(1)));
				}
			});
		//注册变量操作方法type(),返回变量的类型字符串
		scripts.registerVariableFunction("type", new VariableFunction(){

				@Override
				public Object execute(Object variable, List parameters)
				{
					if(Map.class.isInstance(variable)){
						return "map";
					}else if(List.class.isInstance(variable)){
						return "list";
					}else if(Integer.class.isInstance(variable)){
						return "int";
					}else if(Float.class.isInstance(variable)){
						return "float";
					}else if(Boolean.class.isInstance(variable)){	
					return "boolean";
					}else if(CharSequence.class.isInstance(variable)){
						return "string";
					}else{
						return "other";
					}
				}
			});
		//加载脚本
		scripts.loadScriptFile(scriptName,code);


		//运行脚本方法
		Object result= scripts.execute(scriptName,"main",new ArrayList());


		
		}
	
}
