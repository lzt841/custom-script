package com.lzt841.demo;

import com.lzt841.script.language.ls.*;
import com.lzt841.script.language.ls.lexical.*;
import com.lzt841.script.language.ls.model.*;
import java.util.*;
import com.lzt841.script.grammar.*;
import com.lzt841.script.language.ls.result.CalculateResult;
import com.lzt841.script.language.ls.result.OperatorHandler;
import com.lzt841.script.language.ls.utils.*;

public class Test4
{
	/*
	 *实现自定义操作运算
	 *描述：自定义操作运算符，增加一个复合运算符，实现 自定义‘$‘符，达到 10$2得到14的结果，就是左边加上两倍的右边的效果
	 *示例: 1. 
	 *     	123$23=169   >>>  123+2*23=169
	 *	   2.
	 *      45$4=53      >>> 45+2*4=53
	 */
	public static void main(String[] args)
	{
		String scriptName="test";

		//测试用例代码脚本
		String code=
			"function main(){"
			+"a=123$23 \n"
			+"b=45$4\n"
			+"printf('123$23='+a+'\n')"
			+"printf('45$4='+b+'\n')"
			+"}";


		LsEnvironment scripts=new LsEnvironment();
		//第一步:获取词法分析器，注入一个$符合的词；给这个$一个token，这个token要唯一，随便取，取200以外好，要保证唯一就行，例子里就2010好了
		LsLexicalAnalysis analysis= scripts.getLexicalHandler();
		final int token=2010;
		//添加词处理程序进去
		analysis.addCharHandler(new LsSymbolCharHandler(new Keyword("$",token)));
		
		CalculateResult.OperatorValue operatorValue=new CalculateResult.OperatorValue(){

			@Override
			public int getOperatorValue(LsExecuteContext context, ScriptFile scriptFile, Map<String, Object> variableMap, List<CodeNode> codeNodes, int start, List<Value> operatorValuesOut, int operatorToken, OperatorHandler operatorHandler)
			{
				//start为当前运算符的位置，要拿到下一位值，所以start+1
				Value value= context.getValue(scriptFile,variableMap,codeNodes,start+1);
				//将运算符token设置到值缓存里
				value.operatorToken=operatorToken;
				//将value放到预运算队列上
				operatorValuesOut.add(value);
				//返回值所在的指针，好让程序继续往下走
				
				return value.start;
			}
			

			@Override
			public boolean canOperator(LsExecuteContext context,int operatorToken)
			{
				// 当运算符token和自定义token相同时运算
				return token==operatorToken;
			}

			
			@Override
			public int getOperatorLevel(LsExecuteContext context,int operatorToken)
			{
				// 得到运算符的优先级，我们让其和*/同级
				return context.operatorSymbolConfig.MULTIPLY.operatorLevel;
			}

			@Override
			public int skipOperatorValue(LsExecuteContext context, List<CodeNode> codeNodes, int start, int operatorToken)
			{
				// 跳过运算,我们这个运算只对下一位运算，那就跳到下一位值的地方
				return context.skipValue(codeNodes,start+1);
			}

			@Override
			public Object operator(LsExecuteContext context,Value left, int operatorToken, Value right)
			{
				
				//开始运算，返回结果，我们实现左边加上两倍的右边的效果
				int value= CalculateUtils.toInteger(left.value)+2*CalculateUtils.toInteger(right.value);
				return value;
			}
		};
		
		//将运算处理值程序放入到运算程序中
		scripts.getExecuteContext().getResultHandler().getOperatorValues().add(operatorValue);
		
		//加载脚本
		scripts.loadScriptFile(scriptName,code);


		//运行脚本方法
		Object result= scripts.execute(scriptName,"main",new ArrayList());



	}
}
