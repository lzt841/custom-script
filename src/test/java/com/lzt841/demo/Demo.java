package com.lzt841.demo;

import com.lzt841.script.language.ls.*;
import com.lzt841.script.language.ls.model.*;
import java.io.*;
import java.util.*;

public class Demo
{
	/*
		global :指全局，可以作用在变量和方法上，且其他脚本都共享
		例如:  global.list[0]={}
			  global.printf('xxx')
		self：和global一样，只不过作用域不一样，只针对当前脚本内的方法和变量
		 例子： self.map['key'][0]=1
			  self.main()
		循环:
		for循环：（for的（）括号不能去掉）
		1.for(xxx;xxx;xxx){//标准for
			}
		  for(xxx;xxx;)
		  	xxxx
		  
		2.for(x:map){//遍历对象
			
		}
		  for(x:map)
		  	xxxxx
		3.for(x:list){//遍历list
			
		}
		  for(x:list)
		     xxxxxx
		while循环:
		1.while(xxx){
			
		}
		2.while xxxx{
			
		}
		3. while xxxx  xxxxx
		
		do while循环:
		1.do{
			
		}while(xxxx)
		{}和（）可以去掉
		
		if判断:
		1.	if xxxx
				xxxxxxx
		2.  if xxxx
				Xxxxx
			else
				xxxxx
		3.  if xxx
				xxxxx
			else if xxxx
				xxxxx
			else if xx
			.....
		{}和（）可以去掉
		
		三目运算:
			xxxx?aaaaa:bbbb
		
		操作运算:
		+-*！%/&>><<==...都支持
		内置有少许函数，可在com.lzt841.script.language.ls.function.instances下查看具体函数
		可以自定义关键字，自定义操作运算，自定义函数，自定义基本类型，自定义命令；
		还有就是脚本效率有点低下。(●—●)
	*/
	
	public static void main(String[] args)
	{
		String scriptName="test";
        String path = System.getProperty("user.dir")+"/demo.ls";
        System.out.println(path);
		String code=read(path);
					
		//实例化脚本环境
		LsEnvironment scripts=new LsEnvironment();
		//环境加载脚本文件
		Result initResult=scripts.loadScriptFile(scriptName,code);
		
		
		
		Long start=System.currentTimeMillis();
		//执行脚本文件方法
		Object funResult= scripts.execute(scriptName,"main",new ArrayList());
		
		Long end=System.currentTimeMillis();
		System.out.println("结果:"+funResult);
		System.out.println("总共耗时"+((end-start)/1000f)+"秒");
		
		
		
	}
	

    public static String read(String path) {
        BufferedReader reader = null;
		StringBuilder builder=new StringBuilder();
        try {
            reader = new BufferedReader(
				new InputStreamReader(new FileInputStream(path), "UTF-8"));
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line).append("\n");
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
		return builder.toString();
    }
	 
}
